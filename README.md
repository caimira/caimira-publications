# CAiMIRA - CERN Airborne Model for Risk Assessment

CAiMIRA is a risk assessment tool developed to model the concentration of viruses in enclosed spaces, in order to inform space-management decisions.

CAiMIRA models the concentration profile of potential virions in enclosed spaces , both as background (room) concentration and during close-proximity interations, with clear and intuitive graphs.
The user can set a number of parameters, including room volume, exposure time, activity type, mask-wearing and ventilation.
The report generated indicates how to avoid exceeding critical concentrations and chains of airborne transmission in spaces such as individual offices, meeting rooms and labs.

The risk assessment tool simulates the airborne spread SARS-CoV-2 virus in a finite volume, assuming a homogenous mixture and a two-stage exhaled jet model, and estimates the risk of COVID-19 infection therein.
The results DO NOT include the other known modes of SARS-CoV-2 transmission, such as fomite or blood-bound.
Hence, the output from this model is only valid when the other recommended public health & safety instructions are observed, such as good hand hygiene and other barrier measures.

The model used is based on scientific publications relating to airborne transmission of infectious diseases, dose-response exposures and aerosol science, as of February 2022.
It can be used to compare the effectiveness of different airborne-related risk mitigation measures.

Note that this model applies a deterministic approach, i.e., it is assumed at least one person is infected and shedding viruses into the simulated volume.
Nonetheless, it is also important to understand that the absolute risk of infection is uncertain, as it will depend on the probability that someone infected attends the event.
The model is most useful for comparing the impact and effectiveness of different mitigation measures such as ventilation, filtration, exposure time, physical activity, amount and nature of close-range interactions and
the size of the room, considering both long- and short-range airborne transmission modes of COVID-19 in indoor settings.

This tool is designed to be informative, allowing the user to adapt different settings and model the relative impact on the estimated infection probabilities.
The objective is to facilitate targeted decision-making and investment through comparisons, rather than a singular determination of absolute risk.
While the SARS-CoV-2 virus is in circulation among the population, the notion of 'zero risk' or 'completely safe scenario' does not exist.
Each event modelled is unique, and the results generated therein are only as accurate as the inputs and assumptions.

## Authors

CAiMIRA was developed by following members of CERN - European Council for Nuclear Research (visit https://home.cern/):

Andre Henriques<sup>1</sup>, Luis Aleixo<sup>1</sup>, Marco Andreini<sup>1</sup>, Gabriella Azzopardi<sup>2</sup>, James Devine<sup>3</sup>, Philip Elson<sup>4</sup>, Nicolas Mounet<sup>2</sup>, Markus Kongstein Rognlien<sup>2,6</sup>, Nicola Tarocco<sup>5</sup>

<sup>1</sup>HSE Unit, Occupational Health & Safety Group, CERN<br>
<sup>2</sup>Beams Department, Accelerators and Beam Physics Group, CERN<br>
<sup>3</sup>Experimental Physics Department, Safety Office, CERN<br>
<sup>4</sup>Beams Department, Controls Group, CERN<br>
<sup>5</sup>Information Technology Department, Collaboration, Devices & Applications Group, CERN<br>
<sup>6</sup>Norwegian University of Science and Technology (NTNU)<br>

### Reference and Citation

**For the use of the CAiMIRA web app**

CAiMIRA – CERN Airborne Model for Indoor Risk Assessment tool

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6520431.svg)](https://doi.org/10.5281/zenodo.6520431)

© Copyright 2020-2021 CERN. All rights not expressly granted are reserved.

**For use of the CAiMIRA model**

Henriques A, Mounet N, Aleixo L, Elson P, Devine J, Azzopardi G, Andreini M, Rognlien M, Tarocco N, Tang J. (2022). Modelling airborne transmission of SARS-CoV-2 using CARA: risk assessment for enclosed spaces. _Interface Focus 20210076_. https://doi.org/10.1098/rsfs.2021.0076

Reference on the Short-range expiratory jet model from:
Jia W, Wei J, Cheng P, Wang Q, Li Y. (2022). Exposure and respiratory infection risk via the short-range airborne route. _Building and Environment_ _219_: 109166.
https://doi.org/10.1016/j.buildenv.2022.109166

## Installation and running instructions

### Prerequisites

Before proceeding, ensure you have the following installed:

- Python (version 3.6 or higher)
- Git

### Submodules

This repository might have more than one source folder, corresponding to different publications. Each folder targets a specific CAiMIRA version/branch. The required steps to initialize and update the submodules are as follows:

```
git submodule update --init --recursive
```

Once the submodules are initialized, choose a traget folder corresponding to a specific publication and install the required Python packages. For example, to run the results for the `Short-range_paper`, first navigate to the respective directory:

```
cd Short-Range_paper
```

Then, navigate to the CAiMIRA submodule and install the required Python packages:

```
cd caimira
pip install -r requirements.txt
```

Finally, return to the publication's root directory:

```
cd ..
```

### Extra packages

**Note**: One extra package, `tqdm`, is used to visualize the generation of the results. Ensure it is installed:

```
pip install tqdm
``` 

## Obtaining the results

Once the code is properly installed on your machine, you can now run the script to reproduce the results.
All the methods are documented with their respective `docstrings`.

<!-- ### Reproduce one specific figure -->

Note that by **default** all the results are "commented out" for performance reasons. In order to generate a specific result or figure, uncomment the respective generation code by following the steps below (under the respective paper's folder):

- Open the `results_paper.py` file.
  **Note1**: For macOS we recommend to use `IDLE` code editor: right click on the file and select `Open with` and choose `IDLE`.
  **Note2**: For windows we recommend to simply use `Notepad` or other code editor application for pyhton.

- Uncomment the desired code lines for a specific method, by removing the `#` tag before and after the method call.

- For macOS, simply click F5 (Run Module) to run the code if you are using `IDLE`. For windows, simply double click on `results_paper.py`.

- The desired figure or result is generated and it will be displayed in a new window or directly in the terminal.

## Disclaimer

CAiMIRA has not undergone review, approval or certification by competent authorities, and as a result, it cannot be considered as a fully endorsed and reliable tool, namely in the assessment of potential viral emissions from infected hosts to be modelled.

The software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and non-infringement.
In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.
