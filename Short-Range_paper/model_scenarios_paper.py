""" Title: CAiMIRA - CERN Airborne Model for Indoor Risk Assessment
Author: A. Henriques et al
Date: 28/06/2024
Code version: 4.15.3
Availability: https://gitlab.cern.ch/caimira/caimira-publications """

from dataclasses import dataclass
import numpy as np
import typing

from caimira import models, data
from caimira.store.data_registry import DataRegistry
from caimira.monte_carlo.data import activity_distributions, virus_distributions, short_range_expiration_distributions, short_range_distances, expiration_BLO_factors, expiration_distribution, param_evaluation
import caimira.monte_carlo as mc
from caimira.monte_carlo.data import viable_to_RNA_ratio_distribution, infectious_dose_distribution


data_registry: DataRegistry = DataRegistry()

def expiration_distributions(data_registry):
    return {
        exp_type: expiration_distribution(
            data_registry=data_registry,
            BLO_factors=BLO_factors,
            d_min=param_evaluation(data_registry.expiration_particle['long_range_particle_diameter'], 'minimum_diameter'),
            d_max=20 # Changed from 30 to 20 micron - additional evidence
        )
        for exp_type, BLO_factors in expiration_BLO_factors(data_registry).items()
    }

def build_expiration(data_registry, expiration_definition) -> mc._ExpirationBase:
    if isinstance(expiration_definition, str):
        return expiration_distributions(data_registry)[expiration_definition]
    elif isinstance(expiration_definition, dict):
        total_weight = sum(expiration_definition.values())
        BLO_factors = np.sum([
            np.array(expiration_BLO_factors(data_registry)[exp_type]) * weight/total_weight
            for exp_type, weight in expiration_definition.items()
            ], axis=0)
        return expiration_distribution(data_registry=data_registry, 
                                       BLO_factors=tuple(BLO_factors),
                                       d_min=param_evaluation(data_registry.expiration_particle['long_range_particle_diameter'], 'minimum_diameter'),
                                       d_max=20) # Changed from 30 to 20 micron - additional evidence

def short_range_build_expiration(data_registry, expiration_definition) -> mc._ExpirationBase:
    if isinstance(expiration_definition, str):
        return short_range_expiration_distributions(data_registry)[expiration_definition]
    elif isinstance(expiration_definition, dict):
        total_weight = sum(expiration_definition.values())
        BLO_factors = np.sum([
            np.array(expiration_BLO_factors(data_registry)[exp_type]) * weight/total_weight
            for exp_type, weight in expiration_definition.items()
            ], axis=0)
        return expiration_distribution(data_registry=data_registry, 
                                       BLO_factors=tuple(BLO_factors), 
                                       d_min=param_evaluation(data_registry.expiration_particle['short_range_particle_diameter'], 'minimum_diameter'),
                                       d_max=param_evaluation(data_registry.expiration_particle['short_range_particle_diameter'], 'maximum_diameter'))

######### Plot material #########

@dataclass(frozen=True)
class SimpleShortRangeModel:
    '''
    Based on the two-stage (jet/puff) expiratory jet model by 
    Jia et al (2022) - https://doi.org/10.1016/j.buildenv.2022.109166
    '''
    # Pre-defined Long-range concentration
    long_range_concentration: float

    #: Expiration type
    expiration: models._ExpirationBase

    #: Activity type
    activity: models.Activity

    #: Short-range expiration and respective presence
    presence: models.SpecificInterval

    #: Interpersonal distances
    distance: models._VectorisedFloat

    #: Use the mean of aerosols in the intermediate calculations
    with_mean_aerosols: bool

    def dilution_factor(self, u0_input: typing.Union[float, None] = None) -> models._VectorisedFloat:
        '''
        The dilution factor for the respective expiratory activity type.
        '''
        # Average mouth opening diameter (m)
        mouth_diameter = 0.02

        # Breathing rate, from m3/h to m3/s
        BR = np.array(self.activity.exhalation_rate)/3600

        # Exhalation coefficient. Ratio between the duration of a breathing cycle and the duration of
        # the exhalation.
        φ = 2

        # Area of the mouth assuming a perfect circle (m2)
        Am = np.pi*(mouth_diameter**2)/4

        # Exhalation airflow, as per Jia et al. (2022)
        Q_exh = φ * BR if u0_input == None else u0_input * Am

        # Initial velocity of the exhalation airflow (m/s)
        u0 = np.array(Q_exh/Am) if u0_input == None else u0_input

        # Duration of the expiration period(s), assuming a 4s breath-cycle
        tstar = 2.0

        # Streamwise and radial penetration coefficients
        𝛽r1 = 0.18
        𝛽r2 = 0.2
        𝛽x1 = 2.4

        # Parameters in the jet-like stage
        # Position of virtual origin
        x0 = mouth_diameter/2/𝛽r1
        # Time of virtual origin
        t0 = (np.sqrt(np.pi)*(mouth_diameter**3))/(8*(𝛽r1**2)*(𝛽x1**2)*Q_exh)
        # The transition point, m
        xstar = np.mean(np.array(𝛽x1*(Q_exh*u0)**0.25*(tstar + t0)**0.5 - x0))
        # Dilution factor at the transition point xstar
        Sxstar = np.array(2*𝛽r1*(xstar+x0)/mouth_diameter)

        distances = np.array(self.distance)
        factors = np.empty(distances.shape, dtype=np.float64)
        factors[distances < xstar] = 2*𝛽r1*(distances[distances < xstar]
                                            + x0)/mouth_diameter
        factors[distances >= xstar] = Sxstar[distances >= xstar]*(1 +
                                                                  𝛽r2*(distances[distances >= xstar] -
                                                                       xstar[distances >= xstar])/𝛽r1/(xstar[distances >= xstar]
                                                                                                       + x0))**3
        return factors, xstar, u0

    def _short_range_viability_decay(self, room_humidity: float):

        # Average mouth opening diameter (m)
        mouth_diameter: float = 0.02

        # Breathing rate, from m3/h to m3/s
        BR = np.array(self.activity.exhalation_rate/3600.)

        # Exhalation coefficient. Ratio between the duration of a breathing cycle and the duration of
        # the exhalation.
        φ: float = 2

        # Exhalation airflow, as per Jia et al. (2022)
        Q_exh: models._VectorisedFloat = φ * BR

        # Area of the mouth assuming a perfect circle (m2)
        Am = np.pi*(mouth_diameter**2)/4

        # Initial velocity of the exhalation airflow (m/s)
        u0 = np.mean(np.array(Q_exh/Am))

        x = self.distance

        # Time (s) and distace x (m), based on the velocity (m/s)
        tx = x / u0

        if (room_humidity <= .4):
            lambda_sr = -0.016 * tx + 1
        else:
            lambda_sr = 1
        return lambda_sr

    def _long_range_normed_concentration(self, viral_load: models._VectorisedFloat, f_inf: models._VectorisedFloat) -> models._VectorisedFloat:
        """
        Virus long-range exposure concentration normalized by the 
        virus viral load, as function of time.
        """
        return (self.long_range_concentration / self.normalization_factor(viral_load, f_inf)) # mL/m^3 -> mL/cm^3

    def _normed_concentration(self, viral_load: models._VectorisedFloat, f_inf: models._VectorisedFloat, room_humidity: models._VectorisedFloat) -> models._VectorisedFloat:
        """
        Virus short-range exposure concentration.
        """
        dilution = self.dilution_factor()[0]
        if self.with_mean_aerosols:
            jet_origin_concentration = np.mean(self.expiration.aerosols(mask=models.Mask.types['No mask'])) # mL/cm^3
        else:
            jet_origin_concentration = self.expiration.aerosols(mask=models.Mask.types['No mask']) # mL/cm^3

        lambda_sr = self._short_range_viability_decay(room_humidity)

        return ((1/dilution)*(lambda_sr * jet_origin_concentration - self._long_range_normed_concentration(viral_load, f_inf)))

    def short_range_concentration(self, viral_load: models._VectorisedFloat, f_inf: models._VectorisedFloat, room_humidity: models._VectorisedFloat) -> models._VectorisedFloat:
        """
        Virus short-range exposure concentration, as a function of time.
        """
        return (self._normed_concentration(viral_load, f_inf, room_humidity) *
                self.normalization_factor(viral_load, f_inf)) # mL/cm^3 -> mL/m^3
    
    def normalization_factor(self, viral_load: models._VectorisedFloat, f_inf: models._VectorisedFloat) -> models._VectorisedFloat:
        """
        Normalization by the viral load and fraction of infectious virus.
        Note the 10**6 is applied to convert from mL/cm^3 -> mL/m^3.
        """
        return viral_load * f_inf * 10**6

    def concentration(self, viral_load: models._VectorisedFloat, f_inf: models._VectorisedFloat, room_humidity: float) -> models._VectorisedFloat:
        """
        Virus exposure concentration, as a function of time.

        It considers the long-range concentration with the
        contribution of the short-range concentration.
        """
        return self.short_range_concentration(viral_load, f_inf, room_humidity) + self.long_range_concentration

######### Standard ShortRangeModel ###########


def short_range_model(long_range_concentration: float, 
                      expiration: typing.Union[str, models.Expiration], 
                      activity: models._VectorisedFloat, 
                      presence: models.SpecificInterval, 
                      distance: float,
                      with_mean_aerosols: bool = False,
                      SAMPLE_SIZE: typing.Optional[int] = 250_000):
    
    if isinstance(expiration, str):
        exp_dist = short_range_expiration_distributions(data_registry)[expiration].build_model(SAMPLE_SIZE)
    else:
        exp_dist = expiration

    return SimpleShortRangeModel(
        long_range_concentration=long_range_concentration,
        expiration=exp_dist,
        activity=activity,
        presence=models.SpecificInterval(presence),  # Not relevant
        distance=distance,
        with_mean_aerosols=with_mean_aerosols,
    )


######### Standard CO2ConcentrationModel ###########


def simulation_CO2_day() -> models.CO2ConcentrationModel:
    return mc.CO2ConcentrationModel(
        data_registry=data_registry,
        room=models.Room(volume=59.787, inside_temp=models.PiecewiseConstant(
            (0, 24), (21 + 273.15,)), humidity=0.642),  # 21 degrees celsius
        ventilation=models.MultipleVentilation(
            ventilations=[
                models.AirChange(
                    active=models.PeriodicInterval(period=120, duration=120),
                    air_exch=0.25),
                models.SlidingWindow(
                    data_registry=data_registry,
                    active=models.PeriodicInterval(
                        period=1200, duration=163, start=10.17),
                    # https://www.wunderground.com/history/daily/ch/geneva/LSGG/date/2022-10-14
                    outside_temp=models.PiecewiseConstant((0, 24), (278.54, )),
                    window_height=1.60,
                    opening_length=0.25,
                ),
                models.SlidingWindow(
                    data_registry=data_registry,
                    active=models.PeriodicInterval(
                        period=1200, duration=180, start=14.5),
                    # https://www.wunderground.com/history/daily/ch/geneva/LSGG/date/2022-10-14 Avg. 59.71 F
                    outside_temp=models.PiecewiseConstant((0, 24), (288.54, )),
                    window_height=1.60,
                    opening_length=0.1,
                ),
            ]
        ),
        CO2_emitters=mc.Population(
            number=2,
            presence=models.SpecificInterval(((8.63, 11.95), (12.42, 17.5)), ),
            mask=models.Mask.types['No mask'],
            activity=activity_distributions(data_registry)['Seated'],
            host_immunity=0.,
        ),
    )


######### Standard ExposureModel ###########


def std_exposure_model() -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=50, humidity=0.6, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=mc.SpecificInterval(present_times=((0, 8), )),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Seated'],
                expiration=expiration_distributions(data_registry)['Breathing'],
                host_immunity=0.,
            )
        ),
        short_range=(),
        exposed=mc.Population(
            number=3,
            presence=mc.SpecificInterval(present_times=((0, 8), )),
            activity=activity_distributions(data_registry)['Seated'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )


########## Baseline scenarios ###########

# Shared office


def shared_office(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=50, humidity=0.6, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.SlidingWindow(
                        data_registry=data_registry,
                        active=models.PeriodicInterval(
                            period=120, duration=120),
                        outside_temp=data.GenevaTemperatures['Jun'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=models.SpecificInterval(
                    present_times=((8.5, 12.5), (13.5, 17.5))),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Seated'],
                expiration=build_expiration(data_registry,
                    {'Speaking': 1/3, 'Breathing': 2/3}),
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Speaking'],
                activity=activity_distributions(data_registry)['Seated'],
                presence=mc.SpecificInterval(present_times=((10.5, 11.0), )),
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=3,
            presence=mc.SpecificInterval(
                present_times=((8.5, 12.5), (13.5, 17.5))),
            activity=activity_distributions(data_registry)['Seated'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )


def shared_office_15min(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=50, humidity=0.6, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.SlidingWindow(
                        data_registry=data_registry,
                        active=models.PeriodicInterval(
                            period=120, duration=120),
                        outside_temp=data.GenevaTemperatures['Jun'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=models.SpecificInterval(
                    present_times=((8.5, 12.5), (13.5, 17.5))),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Seated'],
                expiration=build_expiration(data_registry,
                    {'Speaking': 1/3, 'Breathing': 2/3}),
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Speaking'],
                activity=activity_distributions(data_registry)['Seated'],
                presence=mc.SpecificInterval(present_times=((10.5, 10.75), )),
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=3,
            presence=mc.SpecificInterval(
                present_times=((8.5, 12.5), (13.5, 17.5))),
            activity=activity_distributions(data_registry)['Seated'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )


# Classroom


def classroom(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=160, humidity=0.4, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.SlidingWindow(
                        active=models.PeriodicInterval(
                            period=60, duration=60),
                        outside_temp=data.GenevaTemperatures['Jan'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                    models.SlidingWindow(
                        active=models.PeriodicInterval(
                            period=60, duration=60),
                        outside_temp=data.GenevaTemperatures['Jan'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=models.SpecificInterval(
                    present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Light activity'],
                expiration=expiration_distributions(data_registry)['Speaking'],
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Speaking'],
                activity=activity_distributions(data_registry)['Light activity'],
                presence=mc.SpecificInterval(present_times=(
                    (9, 9.33), )),  # 20 min is 0.33 of the hour
                distance=short_range_distances(data_registry),
            ),
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Breathing'],
                activity=activity_distributions(data_registry)['Light activity'],
                presence=mc.SpecificInterval(present_times=(
                    (15, 15.25), )),  # 15 min is 0.25 of the hour
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=19,
            presence=mc.SpecificInterval(
                present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
            activity=activity_distributions(data_registry)['Seated'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )


# Patient ward


def patient_ward(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=48, humidity=0.3, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=3),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=4,
                presence=models.SpecificInterval(
                    present_times=((0, 24), ),
                ),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Seated'],
                expiration=build_expiration(data_registry,
                    {'Speaking': 0.5, 'Breathing': 0.5}),
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Speaking'],
                activity=activity_distributions(data_registry)['Light activity'],
                presence=mc.SpecificInterval(
                    # 4 * 15 min (15 min each visit). 15 min is 0.25 h
                    present_times=((8.10, 8.35), (11.10, 11.35), (14.10, 14.35), (17.10, 17.35))), 
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=1,
            presence=mc.SpecificInterval(
                # 4 * 30min over a full shift
                present_times=((8, 8.5), (11, 11.5), (14, 14.5), (17, 17.5), ),
            ),
            activity=activity_distributions(data_registry)['Light activity'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )


# ICU

def ICU(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=64, humidity=0.3, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=6),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=6,
                presence=models.SpecificInterval(
                    present_times=((0, 24), ),
                ),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Seated'],
                expiration=expiration_distributions(data_registry)['Breathing'],
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_build_expiration(data_registry, {'Speaking': 0.25, 'Breathing': 0.75}),
                activity=activity_distributions(data_registry)['Seated'],
                presence=mc.SpecificInterval(present_times=(
                    (10, 11.5), )),  # 15 min per hour. In 8 h it is 2h of short-range interactions 
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=6, # 1 nurse per patient 
            presence=mc.SpecificInterval(
                present_times=((8, 12), (13, 17), ), # 8h shift with 1 h break 
            ),
            activity=activity_distributions(data_registry)['Light activity'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )


######### Modified scenarios #########

# Patient ward modified
def patient_ward_FFP2(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=48, humidity=0.3, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=3),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=4,
                presence=models.SpecificInterval(
                    present_times=((0, 24), ),
                ),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Seated'],
                expiration=build_expiration(data_registry,
                    {'Speaking': 0.5, 'Breathing': 0.5}),
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Speaking'],
                activity=activity_distributions(data_registry)['Light activity'],
                presence=mc.SpecificInterval(
                    # 4 * 15 min (15 min each visit). 15 min is 0.25 h
                    present_times=((8.10, 8.35), (11.10, 11.35), (14.10, 14.35), (17.10, 17.35))), 
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=1,
            presence=mc.SpecificInterval(
                # 4 * 30min over a full shift
                present_times=((8, 8.5), (11, 11.5), (14, 14.5), (17, 17.5), ),
            ),
            activity=activity_distributions(data_registry)['Light activity'],
            mask=models.Mask.types['FFP2'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )

# Shared office modified

def shared_office_modified() -> models.ExposureModel:
    seated_dist: models._VectorisedFloat = activity_distributions(data_registry)['Seated']
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=50, humidity=0.6, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.SlidingWindow(
                        data_registry=data_registry,
                        active=models.PeriodicInterval(
                            period=120, duration=120),
                        outside_temp=data.GenevaTemperatures['Jun'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=models.SpecificInterval(
                    present_times=((8.5, 12.5), (13.5, 17.5))),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=seated_dist,
                expiration=expiration_distributions(data_registry)['Speaking'],
                host_immunity=0.,
            ),
            evaporation_factor = 0.3,
        ),
        short_range=(),
        exposed=mc.Population(
            number=3,
            presence=mc.SpecificInterval(
                present_times=((8.5, 12.5), (13.5, 17.5))),
            activity=seated_dist,
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )

def shared_office_modified_super() -> models.ExposureModel:
    seated_dist: models._VectorisedFloat = activity_distributions(data_registry)['Seated']
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=50, humidity=0.6, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.SlidingWindow(
                        data_registry=data_registry,
                        active=models.PeriodicInterval(
                            period=120, duration=120),
                        outside_temp=data.GenevaTemperatures['Jun'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=models.SpecificInterval(
                    present_times=((8.5, 12.5), (13.5, 17.5))),
                virus=mc.SARSCoV2(
                    viral_load_in_sputum=10**9, # Modify as desired
                    infectious_dose=infectious_dose_distribution(),
                    viable_to_RNA_ratio=viable_to_RNA_ratio_distribution(),
                    transmissibility_factor=0.2, # For SARS_CoV_2 OMICRON 
                ),
                mask=models.Mask.types['No mask'],
                activity=seated_dist,
                expiration=expiration_distributions(data_registry)['Speaking'],
                host_immunity=0.,
            ),
            evaporation_factor = 0.3,
        ),
        short_range=(),
        exposed=mc.Population(
            number=3,
            presence=mc.SpecificInterval(
                present_times=((8.5, 12.5), (13.5, 17.5))),
            activity=seated_dist,
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )

# Classroom modified

def classroom_modified(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=160, humidity=0.4, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.SlidingWindow(
                        active=models.PeriodicInterval(
                            period=60, duration=60),
                        outside_temp=data.GenevaTemperatures['Jan'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                    models.SlidingWindow(
                        active=models.PeriodicInterval(
                            period=60, duration=60),
                        outside_temp=data.GenevaTemperatures['Jan'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=models.SpecificInterval(
                    present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Light activity'],
                expiration=expiration_distributions(data_registry)['Speaking'],
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Speaking'],
                activity=activity_distributions(data_registry)['Light activity'],
                presence=mc.SpecificInterval(
                    present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=19,
            presence=mc.SpecificInterval(
                present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
            activity=activity_distributions(data_registry)['Seated'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )

def classroom_modified_FFP2(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=160, humidity=0.4, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.SlidingWindow(
                        active=models.PeriodicInterval(
                            period=60, duration=60),
                        outside_temp=data.GenevaTemperatures['Jan'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                    models.SlidingWindow(
                        active=models.PeriodicInterval(
                            period=60, duration=60),
                        outside_temp=data.GenevaTemperatures['Jan'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=models.SpecificInterval(
                    present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['No mask'],
                activity=activity_distributions(data_registry)['Light activity'],
                expiration=expiration_distributions(data_registry)['Speaking'],
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Speaking'],
                activity=activity_distributions(data_registry)['Light activity'],
                presence=mc.SpecificInterval(
                    present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=19,
            presence=mc.SpecificInterval(
                present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
            activity=activity_distributions(data_registry)['Seated'],
            mask=models.Mask.types['FFP2'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )

def classroom_modified_SM(peak_short_range: bool = False) -> models.ExposureModel:
    return mc.ExposureModel(
        data_registry=data_registry,
        concentration_model=mc.ConcentrationModel(
            data_registry=data_registry,
            room=mc.Room(volume=160, humidity=0.4, inside_temp=mc.PiecewiseConstant(
                (0, 24), (20+273.15, ))),
            ventilation=mc.MultipleVentilation(
                ventilations=[
                    models.AirChange(active=models.PeriodicInterval(
                        period=120, duration=120), air_exch=0.25),
                    models.SlidingWindow(
                        active=models.PeriodicInterval(
                            period=60, duration=60),
                        outside_temp=data.GenevaTemperatures['Jan'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                    models.SlidingWindow(
                        active=models.PeriodicInterval(
                            period=60, duration=60),
                        outside_temp=data.GenevaTemperatures['Jan'],
                        window_height=1.6,
                        opening_length=0.2,
                    ),
                ]
            ),
            infected=mc.InfectedPopulation(
                data_registry=data_registry,
                number=1,
                presence=models.SpecificInterval(
                    present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
                virus=virus_distributions(data_registry)['SARS_CoV_2_OMICRON'],
                mask=models.Mask.types['Type I'],
                activity=activity_distributions(data_registry)['Light activity'],
                expiration=expiration_distributions(data_registry)['Speaking'],
                host_immunity=0.,
            ),
            evaporation_factor=0.3,
        ),
        short_range=(
            mc.ShortRangeModel(
                data_registry=data_registry,
                expiration=short_range_expiration_distributions(data_registry)['Breathing'], # equivalent to leaks from SM
                activity=activity_distributions(data_registry)['Seated'], # equivalent to leaks from SM
                presence=mc.SpecificInterval(
                    present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
                distance=short_range_distances(data_registry),
            ),
        ) if peak_short_range else (),
        exposed=mc.Population(
            number=19,
            presence=mc.SpecificInterval(
                present_times=((8.5, 10), (10.25, 12.5), (13.5, 15.), (15.25, 17.5),)),
            activity=activity_distributions(data_registry)['Seated'],
            mask=models.Mask.types['No mask'],
            host_immunity=0.,
        ),
        geographical_data=(),
    )

######### Case scenarios for conditional probability #########

def cases_model(geographic_population: int, geographical_cases: int, ascertainment_bias: int):
    return models.Cases(
        geographic_population=geographic_population,
        geographic_cases=geographical_cases,
        ascertainment_bias=ascertainment_bias
    )
