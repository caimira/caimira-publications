""" Title: CAiMIRA - CERN Airborne Model for Indoor Risk Assessment
Author: A. Henriques et al
Date: 28/08/2024
Code version: 4.15.3
Availability: https://gitlab.cern.ch/caimira/caimira-publications """

from scripts_paper import *
from model_scenarios_paper import *


############# Results section ############

#### Emission rate for a given physical activity - vR_SR (jet origin concentration * breathing rate)
### Data
# short_range_emission_rate_values(physical_activity='Standing', expiratory_activity='Breathing', with_mean_aerosols=True, use_f_inf_beta_dist=False) 
# short_range_emission_rate_values(physical_activity='Standing', expiratory_activity='Speaking', with_mean_aerosols=True, use_f_inf_beta_dist=False)
# short_range_emission_rate_values(physical_activity='Standing', expiratory_activity='Shouting', with_mean_aerosols=True, use_f_inf_beta_dist=False)
# short_range_emission_rate_physical_activity_comparison(first_physical_activity='Heavy exercise', 
#                                               second_physical_activity='Standing', 
#                                               expiratory_activity='Speaking',
#                                               with_mean_aerosols=False)
### Fig 2: 
# present_vl_er_histograms(physical_activity='Standing', expiratory_activities=['Breathing', 'Speaking', 'Shouting'], with_mean_aerosols=True, use_f_inf_beta_dist=False)

#### Concentration of the jet - C_SR 
### Data
# short_range_values(activity='Seated', expiration='Breathing', distance=1., room_humidity = 0.3, with_mean_aerosols=False)
# short_range_values(activity='Seated', expiration='Speaking', distance=1., room_humidity = 0.3, with_mean_aerosols=False)
# short_range_values(activity='Seated', expiration='Shouting', distance=1., room_humidity = 0.3, with_mean_aerosols=False)
# dilution_values(activity='Seated', expiration='Breathing', distance=2.)
# dilution_values(activity='Heavy exercise', expiration='Breathing', distance=2.)
### Fig 3: 
# short_range_concentration(long_range_concentration=10, activity='Standing', expiration='Speaking',
#                           max_distance=2, room_humidity=0.5, display_plot_color_code=False, with_mean_aerosols=False)
# dilution_factor(activity='Standing', max_distance=2, full=True)
### Fig 4: 
# dilution_factor(activity='Standing', max_distance=2, full=True)

#### Peak concentration and dose
### Data 
# model_concentration_results(exposure_model=shared_office(peak_short_range=True), CO2_values=False)
# model_concentration_results(exposure_model=shared_office(peak_short_range=False), CO2_values=False)
# model_concentration_results(exposure_model=classroom(peak_short_range=True), CO2_values=False)
# model_concentration_results(exposure_model=classroom(peak_short_range=False), CO2_values=False)
# model_concentration_results(exposure_model=patient_ward(peak_short_range=True), CO2_values=False)
# model_concentration_results(exposure_model=patient_ward(peak_short_range=False), CO2_values=False)
# model_concentration_results(exposure_model=ICU(peak_short_range=True), CO2_values=False)
# model_concentration_results(exposure_model=ICU(peak_short_range=False), CO2_values=False)
# model_concentration_results(exposure_model=patient_ward_FFP2(peak_short_range=True), CO2_values=False)
# model_concentration_results(exposure_model=patient_ward_FFP2(peak_short_range=False), CO2_values=False)

#### SR/LR ratio
### Data
# comparison_sr_lr_threshold(exposure_model=shared_office_modified(), expirations=['Breathing', 'Speaking'], max_distance=15., with_mean_aerosols=False)

##### Dose contribution 
### Data  
# model_dose_results(exposure_models=[shared_office(peak_short_range=True), shared_office_15min(peak_short_range=True)], labels = ['30min SR', '15min SR'])

##### Risk of infection and new cases 
### Fig5:
# heatmap_data(exposure_model=classroom_modified(peak_short_range=True), viral_dose=False, risk_of_infection=True, expected_new_cases=False)
# heatmap_data(exposure_model=classroom_modified(peak_short_range=True), viral_dose=False, risk_of_infection=False, expected_new_cases=True)
# heatmap_data(exposure_model=classroom_modified_FFP2(peak_short_range=True), viral_dose=False, risk_of_infection=False, expected_new_cases=True)
# heatmap_data(exposure_model=classroom_modified_SM(peak_short_range=True), viral_dose=False, risk_of_infection=False, expected_new_cases=True)
### Data  
# model_deterministic_exposure(exposure_models = [shared_office(), classroom()], labels = ['Shared office', 'Classroom'])

###### Buoyant Jet  
### Data
# jet_profile(activity_type='Heavy exercise',
#             expiration_type='Speaking', 
#             initial_vertical_position = 1.6, 
#             max_horizontal_distance = 2., 
#             straight_jet = False,
#             show_initial_points = False,
#             ylimit_max = True,
#             save_png_transparent_background=True)
# jet_profile(activity_type='Standing',
#             expiration_type='Speaking', 
#             initial_vertical_position = 1.6, 
#             max_horizontal_distance = 2., 
#             straight_jet = False,
#             show_initial_points = False,
#             ylimit_max = True,
#             save_png_transparent_background=True)
### Fig6:
# jet_profile(activity_type='Heavy exercise',
#             expiration_type='Speaking', 
#             initial_vertical_position = 1.6, 
#             max_horizontal_distance = 2., 
#             straight_jet = True,
#             show_initial_points = False,
#             ylimit_max = True,
#             save_png_transparent_background=True)
# jet_profile(activity_type='Heavy exercise',
#             expiration_type='Speaking', 
#             initial_vertical_position = 1.6, 
#             max_horizontal_distance = 2., 
#             straight_jet = False,
#             show_initial_points = False,
#             ylimit_max = True,
#             save_png_transparent_background=True)

###### Conditional probability of infected occupants attending
### Data
# model_probabilistic_exposure(exposure_models = [ classroom(), shared_office()],
#                             labels = ['Classroom', 'Shared office'],
#                             cases = cases_model(
#                                 geographic_population = 100_000,
#                                 geographical_cases = 100, 
#                                 ascertainment_bias = 1
#                             ),
#                             show_deterministic_prob = True,
#                             ),
# model_probabilistic_exposure(exposure_models = [shared_office()],
#                             labels = ['Shared office'],
#                             cases = cases_model(
#                                 geographic_population = 100_000,
#                                 geographical_cases = 10, 
#                                 ascertainment_bias = 1
#                             ),
#                             show_deterministic_prob = True,
#                             ),


############# Discussion section ############

#### Comparison with Jones et al.
# short_range_emission_rate_values(physical_activity='Standing', expiratory_activity={'Breathing': 0.75, 'Speaking': 0.25}, with_mean_aerosols=True, use_f_inf_beta_dist=False)

#### Patient ward - HCW with FFP2
# model_concentration_results(exposure_model=patient_ward_FFP2(peak_short_range=True), CO2_values=False)

#### 8h LR - 15min SR comparison (shared office)
# model_dose_results(exposure_models=[shared_office_15min(peak_short_range=True)], labels = ['15min SR; 8h LR'])

# model_probabilistic_exposure(exposure_models = [shared_office_modified_super()],
#                             labels = ['Shared office super-emitter'],
#                             cases = cases_model(
#                                 geographic_population = 100_000,
#                                 geographical_cases = 4000,
#                                 ascertainment_bias = 1
#                             ),
#                             show_deterministic_prob = True,
#                             ),


############# Supplementary material ############
### Fig S.1
# compare_vl_hist()
### Fig S.2
# generate_comparison_sr_lr_curve(exposure_model=shared_office_modified(), expirations=['Breathing', 'Speaking'], max_distance=10, save_png=False, with_mean_aerosols=True)

##### Random (stochastic) variables
# statistics_for_random_variable(physical_activity=False, viral_load=True, mask=False, viable_to_RNA=False, infectious_dose=False, interpersonal_distance=False)

