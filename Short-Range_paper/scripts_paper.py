""" Title: CAiMIRA - CERN Airborne Model for Indoor Risk Assessment
Author: A. Henriques et al
Date: 28/06/2024
Code version: 4.15.3
Availability: https://gitlab.cern.ch/caimira/caimira-publications """

from caimira import models
from model_scenarios_paper import short_range_model, expiration_distributions, short_range_build_expiration, SimpleShortRangeModel
from caimira.monte_carlo.data import short_range_expiration_distributions, activity_distributions, covid_overal_vl_data, symptomatic_vl_frequencies, viable_to_RNA_ratio_distribution, mask_distributions, infectious_dose_distribution, short_range_distances, virus_distributions
from caimira import dataclass_utils
from caimira.store.data_registry import DataRegistry
import caimira.apps.calculator.report_generator as rep_gen
import caimira.monte_carlo as mc
from caimira.monte_carlo.sampleable import Custom

from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib as mpl
from matplotlib.colors import rgb2hex
from scipy.stats import beta
from scipy.integrate import quad

import typing
import dataclasses

SAMPLE_SIZE: int = 250_000
data_registry: DataRegistry = DataRegistry()

############# AUX ############

def print_er_info(er: np.array, log_er: np.array, percentiles: bool = False) -> None:
    """
    Prints statistical parameters of a given distribution of ER-values
    """
    print(f"MEAN of ER = {np.mean(er)}\n"
          f"MEAN of log ER = {np.mean(log_er)}\n"
          f"SD of ER = {np.std(er)}\n"
          f"SD of log ER = {np.std(log_er)}\n"
          f"Median of ER = {np.quantile(er, 0.5)}\n")

    if percentiles:
        print(f"Percentiles of ER:")
        for quantile in (0.01, 0.05, 0.25, 0.50, 0.55, 0.65, 0.75, 0.95, 0.99):
            print(f"ER_{quantile} = {np.quantile(er, quantile)}")

    return None


def define_colormap(factors, color_scale=mpl.cm.Reds):
    min_val, max_val = 0., 0.8
    n = 100
    orig_cmap = color_scale

    c_dist = np.linspace(min_val, max_val, n)
    colors = orig_cmap(c_dist)

    # Create a custom colormap using the modified colors
    custom_cmap = mpl.colors.ListedColormap(colors)

    # Apply LogNorm to the data
    vmin=min(factors)
    vmax=max(factors)
    norm = mpl.colors.LogNorm(vmin if vmin != 0 else 0.00001, vmax)
    return custom_cmap, norm


def define_colormap_linear(cns):
    min_val, max_val = 0.25, 0.85
    n = 10
    orig_cmap = mpl.cm.Reds
    colors = orig_cmap(np.linspace(min_val, max_val, n))

    norm = mpl.colors.Normalize(vmin=cns.min(), vmax=cns.max())
    cmap = mpl.cm.ScalarMappable(
        norm=norm, cmap=mpl.colors.LinearSegmentedColormap.from_list("mycmap", colors))
    cmap.set_array([])

    return cmap


def c_model_start_end(c_model: models.CO2ConcentrationModel):
    t_start = c_model.CO2_emitters.presence.boundaries()[0][0]
    t_end = c_model.CO2_emitters.presence.boundaries()[-1][1]
    return t_start, t_end


def fill_big_gaps(array, gap_size):
    """
    Insert values into the given sorted list if there is a gap of more than ``gap_size``.
    All values in the given array are preserved, even if they are within the ``gap_size`` of one another.

    >>> fill_big_gaps([1, 2, 4], gap_size=0.75)
    [1, 1.75, 2, 2.75, 3.5, 4]

    """
    result = []
    if len(array) == 0:
        raise ValueError("Input array must be len > 0")

    last_value = array[0]
    for value in array:
        while value - last_value > gap_size + 1e-15:
            last_value = last_value + gap_size
            result.append(last_value)
        result.append(value)
        last_value = value
    return result


def non_temp_transition_times(model: models.ExposureModel):
    """
    Return the non-temperature (and PiecewiseConstant) based transition times.

    """
    def walk_model(model, name=""):
        # Extend walk_dataclass to handle lists of dataclasses
        # (e.g. in MultipleVentilation).
        for name, obj in dataclass_utils.walk_dataclass(model, name=name):
            if name.endswith('.ventilations') and isinstance(obj, (list, tuple)):
                for i, item in enumerate(obj):
                    fq_name_i = f'{name}[{i}]'
                    yield fq_name_i, item
                    if dataclasses.is_dataclass(item):
                        yield from dataclass_utils.walk_dataclass(item, name=fq_name_i)
            else:
                yield name, obj

    t_start, t_end = c_model_start_end(model)

    change_times = {t_start, t_end}
    for name, obj in walk_model(model, name="exposure"):
        if isinstance(obj, models.Interval):
            change_times |= obj.transition_times()

    # Only choose times that are in the range of the model (removes things
    # such as PeriodicIntervals, which extend beyond the model itself).
    return sorted(time for time in change_times if (t_start <= time <= t_end))


def interesting_times(model: models.CO2ConcentrationModel, approx_n_pts=100) -> typing.List[float]:
    """
    Pick approximately ``approx_n_pts`` time points which are interesting for the
    given model.

    Initially the times are seeded by important state change times (excluding
    outside temperature), and the times are then subsequently expanded to ensure
    that the step size is at most ``(t_end - t_start) / approx_n_pts``.

    """
    times = non_temp_transition_times(model)

    # Expand the times list to ensure that we have a maximum gap size between
    # the key times.
    nice_times = fill_big_gaps(times, gap_size=(
        max(times) - min(times)) / approx_n_pts)
    return nice_times


def draw_border_line(ax, distances, times, mask, color, linestyle, linewidth, label):
    for i in range(len(distances)):
        for j in range(len(times[1:])):
            if mask[i][j] and (i == 0 or not mask[i-1][j]):
                # Draw horizontal line at the top of the box
                line_start = (j - 0.5, i - 0.5)
                line_end = (j + 0.5, i - 0.5)
                if i != 0:
                    ax.plot([line_start[0], line_end[0]], [line_start[1], line_end[1]], color=color, linestyle=linestyle, linewidth=linewidth)
                
                # Check if the line continues horizontally
                if j + 1 < len(times[1:]) and mask[i][j + 1]:
                    continue
                
                # Draw vertical line at the right side of the box
                line_start = (j + 0.5, i - 0.5)
                line_end = (j + 0.5, i + 0.5)
                if j + 1 < len(times[1:]):
                    ax.plot([line_start[0], line_end[0]], [line_start[1], line_end[1]], color=color, linestyle=linestyle, linewidth=linewidth)
    ax.plot([], [], color=color, linestyle=linestyle, linewidth=linewidth, label=label)
    return


def f_inf_beta_distribution(alpha: float = 2, 
                            beta_param: float = 5, 
                            lower_limit: float = 10**-4, 
                            upper_limit: float = 10**-2,
                            SAMPLE_SIZE: int = SAMPLE_SIZE,
                            render_graph: bool = False):
    
    def custom_pdf(x):
        return beta.pdf((x - lower_limit) / (upper_limit - lower_limit), alpha, beta_param) / (upper_limit - lower_limit)

    # Determine the maximum value of the PDF function
    x_range = np.linspace(lower_limit, upper_limit, 100)
    max_function = np.max(custom_pdf(x_range))

    # Define the custom distribution
    custom_dist = Custom(bounds=(lower_limit, upper_limit),
                        function=custom_pdf,
                        max_function=max_function)

    samples = custom_dist.generate_samples(SAMPLE_SIZE)

    if render_graph:
        mean_scaled = np.mean(samples)
        # Define the range for the PDF calculation
        x = np.linspace(lower_limit, upper_limit, 10000)
        # Compute the PDF of the scaled beta distribution
        pdf = custom_pdf(x)
        # Calculate the maximum density value
        max_density = np.max(pdf)
        # PDF function to integrate
        def pdf_function(x):
            return custom_pdf(x)
        # Area under the PDF curve
        area, _ = quad(pdf_function, lower_limit, upper_limit)

        # Print the area under the PDF curve and maximum density
        print(f"Area under the PDF curve: {area:.4f}")
        print(f"Maximum density value: {max_density:.4f}")

        # Plot
        plt.figure(figsize=(10, 6))
        plt.hist(samples, bins=500, density=True, alpha=0.6, label='Density Histogram')
        plt.plot(x, pdf, 'r-', lw=2, label='Beta PDF')
        plt.axvline(mean_scaled, color='b', linestyle='--', linewidth=2, label=f'Mean: {mean_scaled:.2e}')
        plt.xlabel('Value')
        plt.ylabel('Density')
        plt.title('Density Histogram and PDF of Scaled Beta Distribution')
        plt.legend()
        plt.show()
    return samples


############# METHODS ############


def short_range_values(activity: str, expiration: str, distance: float, room_humidity: float, with_mean_aerosols: bool = False) -> None:
    SAMPLE_SIZE = 250_000 if expiration == "Breathing" else 1_000_000

    # Activity distribution
    activity_dist: models.Activity = activity_distributions(data_registry)[activity].build_model(SAMPLE_SIZE)

    # Short-range expiration distribution
    short_range_exp_dist: models.Expiration = short_range_expiration_distributions(data_registry)[expiration].build_model(SAMPLE_SIZE)

    # Short-range distances
    sr_distances_dist: models._VectorisedFloat = short_range_distances(data_registry).generate_samples(SAMPLE_SIZE)

    # Virus distributions
    virus_dist: models.Virus = virus_distributions(data_registry)['SARS_CoV_2_OMICRON'].build_model(SAMPLE_SIZE)

    # Short-range model, for C_0,sr calculation
    sr_model: models.ShortRangeModel = models.ShortRangeModel(
        data_registry=data_registry,
        expiration=short_range_exp_dist,
        activity=activity_dist,
        presence=models.SpecificInterval(((8, 12),)),  # Not relevant
        distance=sr_distances_dist
    )

    # Infected model
    infected: models.InfectedPopulation = models.InfectedPopulation(
        data_registry=data_registry,
        number=1, # Not relevant
        presence=models.SpecificInterval(((8, 12),)),  # Not relevant
        activity=activity_dist,
        mask=models.Mask.types['No mask'],
        host_immunity=0.,
        virus=virus_dist,
        expiration=[0] * SAMPLE_SIZE 
    )
     
    # Concentration at jet origin
    if with_mean_aerosols: 
        # np.mean(aerosols) correspond to the MC integration of the particle diameters. 
        # In order to return the results in IRP / m3 (instead of IRP / (m3.µm))
        aerosols: models._VectorisedFloat = short_range_exp_dist.aerosols(models.Mask.types['No mask'])
        C0_sr: models._VectorisedFloat = sr_model.jet_origin_concentration(infected) / aerosols * np.mean(aerosols)
        C0_sr_mean: float = np.mean(C0_sr)
    else:
        # without np.mean(aerosols) this returns IRP / (m3.µm)
        C0_sr: models._VectorisedFloat = sr_model.jet_origin_concentration(infected)
        C0_sr_mean: float = np.mean(C0_sr)
    
    #######################################################################
    # For a specific distance, we need a custom SR model for that distance:
    simple_short_range_model: SimpleShortRangeModel = short_range_model(
        long_range_concentration=10, # IRP/m^3
        expiration=short_range_exp_dist,
        activity=activity_dist,
        presence=models.SpecificInterval(((8, 12),)),  # Not relevant
        distance=distance, # From input
        with_mean_aerosols=with_mean_aerosols, # From input
    )

    # Short-range concentration
    sr_concentration: models._VectorisedFloat = simple_short_range_model.concentration(viral_load=virus_dist.viral_load_in_sputum,
                                                                                       f_inf=infected.fraction_of_infectious_virus(),
                                                                                       room_humidity=room_humidity)
    sr_concentration_mean: float = np.mean(sr_concentration)

    print(
        'Physical Activity:', activity,
        '\nExpiratory Activity:', expiration,
        '\nC0_sr: ', C0_sr_mean,
        '\nC0_sr (scientific notation): ', "{:2e}".format(C0_sr_mean),
        '\nCsr (d=', distance, '): ', np.mean(sr_concentration),
        '\nC0_sr / Csr (d=', distance, '):', f'{C0_sr_mean / sr_concentration_mean:.0f}', '- fold',
        '\n>>>>>>>'
        '\n'
    )

    return None


def dilution_values(activity: str, expiration: str, distance: float) -> None:
    SAMPLE_SIZE = 250_000 if expiration == "Breathing" else 1_000_000

    # Activity distribution
    activity_dist: models.Activity = activity_distributions(data_registry)[activity].build_model(SAMPLE_SIZE)

    # Short-range expiration distribution
    short_range_exp_dist: models.Expiration = short_range_expiration_distributions(data_registry)[expiration].build_model(SAMPLE_SIZE)
    
    #######################################################################
    # For a specific distance, we need a custom SR model for that distance:
    short_range: SimpleShortRangeModel = short_range_model(
        long_range_concentration=10,  # Not relevant
        expiration=short_range_exp_dist,
        activity=activity_dist,
        presence=models.SpecificInterval(((8, 12),)),  # Not relevant
        distance=distance,
    )
    
    dilution_value: models._VectorisedFloat = short_range.dilution_factor()[0]
    u0: float = np.mean(short_range.dilution_factor()[2]) # m/s

    print(
        'Physical Activity:', activity,
        '\nExpiratory Activity:', expiration,
        '\nu0 :', u0,
        '\nS(x=2m):', dilution_value,
        '\n>>>>>>>'
    )

    return None


def short_range_factors(distances: models._VectorisedFloat, 
                        long_range_concentration: float,
                        expiration: str,
                        activity: str,
                        room_humidity: float,
                        with_mean_aerosols: bool = False) -> models._VectorisedFloat:

    SAMPLE_SIZE = 250_000 if expiration == "Breathing" else 1_000_000
    factors: models._VectorisedFloat = []
    viral_load: models._VectorisedFloat = covid_overal_vl_data(data_registry).generate_samples(
        SAMPLE_SIZE)
    f_inf: models._VectorisedFloat = viable_to_RNA_ratio_distribution().generate_samples(SAMPLE_SIZE)
    activity_dist: models._VectorisedFloat = activity_distributions(data_registry)[activity].build_model(
        SAMPLE_SIZE)
    short_range: SimpleShortRangeModel = short_range_model(
        long_range_concentration=long_range_concentration,
        expiration=expiration,
        activity=activity_dist,
        presence=models.SpecificInterval(((8, 12),)),  # Not relevant
        distance=0.,
        with_mean_aerosols=with_mean_aerosols,
        SAMPLE_SIZE=SAMPLE_SIZE,
    )
    for d in tqdm(distances):
        short_range_d: SimpleShortRangeModel = dataclass_utils.nested_replace(
            short_range, {
                'distance': d
            },
        )
        factors.append(
            np.mean(short_range_d.concentration(viral_load, f_inf, room_humidity)))
    return factors


def short_range_concentration(long_range_concentration: float, 
                              activity: str, 
                              expiration: str, 
                              max_distance: float, 
                              room_humidity: float, 
                              display_plot_color_code: bool,
                              with_mean_aerosols: bool = False) -> None:
    
    distances: models._VectorisedFloat = np.linspace(0, max_distance, 750)
    factors: models._VectorisedFloat = short_range_factors(distances=distances,
                                                           long_range_concentration=long_range_concentration,
                                                           expiration=expiration,
                                                           activity=activity,
                                                           room_humidity=room_humidity,
                                                           with_mean_aerosols=with_mean_aerosols)

    custom_cmap, norm = define_colormap(factors)

    if display_plot_color_code:
        distances_for_colors = np.concatenate(
            (np.linspace(0, 10.53/100, 5), np.linspace(15.79/100, max_distance, 15)))
        for d in distances_for_colors:
            closest_index = np.argmin(np.abs(distances - d))
            print('Distance: ', format(d * 100 / max_distance, ".2f"), "%",
                  ' Factor: ', factors[closest_index],
                  ' Color: ', [
                      i * 255 for i in custom_cmap(norm(factors[closest_index]))[:3]],
                  ' Hex: ', str(
                      rgb2hex(custom_cmap(norm(factors[closest_index])))[1:])
                  )
        index_distance_at_start = np.argmin(
            np.abs(distances - distances_for_colors[-1]))
        print('Distance at end: ', format(distances[index_distance_at_start] * 100 / max_distance, ".2f"), "%",
              ' Factor: ', factors[index_distance_at_start],
              ' Color (background): ', [
            i * 255 for i in custom_cmap(norm(factors[index_distance_at_start]))[:3]],
            ' Hex: ', str(
                  rgb2hex(custom_cmap(norm(factors[index_distance_at_start])))[1:])
        )

        # Use this line to modify the graph background color
        # plt.figure(facecolor=custom_cmap(norm(factors[index_distance_at_start]))[:3])

        # Use these lines to add the colorbar
        sm = plt.cm.ScalarMappable(cmap=custom_cmap, norm=norm)
        colorbar = plt.colorbar(sm, orientation='horizontal', ax=plt.gca())
        colorbar.ax.invert_xaxis()

    for (d, f) in zip(distances, factors):
        plt.scatter(d, f, c=[custom_cmap(norm(f))], s=5)

    plt.ticklabel_format(style='sci', axis='y', scilimits=(3, 3))
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.xlabel('Distance (m)', fontsize=13)
    plt.ylabel('Viral Concentration (IRP/m³)', fontsize=13)
    plt.show()

    return None


def dilution_factor(activity: str, max_distance: float, full: bool = False) -> None:
    distances: models._VectorisedFloat = np.linspace(0, max_distance, 100)
    activity_dist: models._VectorisedFloat = activity_distributions(data_registry)[activity].build_model(SAMPLE_SIZE)
    if full == True:
        velocities: models._VectorisedFloat = np.linspace(0.5, 5.8, 200)
        cmap = define_colormap_linear(velocities)
        for index, u0 in enumerate(tqdm(velocities)):
            dilutions: list = []
            for d in distances:
                short_range: SimpleShortRangeModel = short_range_model(
                    long_range_concentration=20,  # Not relevant
                    expiration='Speaking',  # Not relevant, replaced by u0
                    activity=activity_dist,
                    presence=((8, 12),),  # Not relevant
                    distance=d,
                    SAMPLE_SIZE=100,
                )
                current_dilution, xstar, u0_new = short_range.dilution_factor(
                    u0_input=u0)
                dilutions.append(current_dilution)
            plt.plot(distances, dilutions,
                     color=cmap.to_rgba(u0_new), linewidth=1)
            
            # Transition point xstar (first one, lowest velocity)
            if index == 0:
                closest_index_xstar = np.argmin(np.abs(np.array(distances) - xstar))
                plt.scatter(distances[closest_index_xstar], dilutions[closest_index_xstar], color='k', marker='*', s=100, label='Jet-puff transition area', zorder=10,)
        
        # Transition point xstar (last one, highest velocity)
        closest_index_xstar = np.argmin(np.abs(np.array(distances) - xstar))
        plt.scatter(distances[closest_index_xstar], dilutions[closest_index_xstar], color='k', marker='*', s=100, zorder=10)

        # Use these lines to add the colorbar
        cmap = plt.colorbar(cmap, ticks=[1, 2, 3, 4, 5], ax=plt.gca())
        cmap.set_ticklabels(
            ['1 (Standing)', '2 (Light intensity)', 3, '4 (Moderate intensity)', 5])
        cmap.set_label(label='Exhale velocity, $u_0$ (m/s)', fontsize=12)
        cmap.ax.yaxis.set_label_position('left')

        index = next(x for x, val in enumerate(distances) if val > xstar)
        plt.plot(distances[:index], dilutions[:index],
                 color='k', ls='--', lw='2.5', label='jet-like stage')
        plt.plot(0, 0, label='puff-like stages - f($u_0$)', color='firebrick')

        plt.yscale('log')
        plt.xlim(0, 2)
        plt.xticks(fontsize=12)
        plt.yticks(fontsize=12)
        plt.xlabel('Distance from jet origin (m)', fontsize=13)
        plt.ylabel('Jet dilution factor', fontsize=13)
        plt.legend()
        plt.show()

    else:
        dilutions: list = []
        for d in distances:
            short_range: SimpleShortRangeModel = short_range_model(
                long_range_concentration=10,  # Not relevant
                expiration='Speaking',
                activity=activity_dist,
                presence=((8, 12),),  # Not relevant
                distance=d
            )
            dilution_factor_output = short_range.dilution_factor()
            dilutions.append(dilution_factor_output[0])

        xstar = dilution_factor_output[1]
        # Transition point xstar
        closest_index_xstar = np.argmin(np.abs(np.array(distances) - xstar))
        plt.scatter(distances[closest_index_xstar], dilutions[closest_index_xstar], color='black', label='Transition point', zorder=10)

        plt.yscale('log')
        plt.xticks(fontsize=12)
        plt.yticks(fontsize=12)
        plt.xlabel('Distance (m)', fontsize=13)
        plt.ylabel('Dilution factor', fontsize=13)
        plt.plot(distances, dilutions, label='Dilution factor')
        plt.legend()
        plt.show()

    return None


def model_concentration_results(exposure_model: models.ExposureModel, CO2_values: bool = True):
    model: models.ExposureModel = exposure_model.build_model(size=SAMPLE_SIZE)
    times: models._VectorisedFloat = rep_gen.interesting_times(
        model, approx_n_pts=1000)

    ############# Peak viral concentration ############
    concentrations: models._VectorisedFloat = [
        np.array(model.concentration(float(time)))
        for time in times
    ]

    peak_concentration_index: int = np.argmax(
        [c.mean() for c in concentrations])

    mean = round(np.mean(concentrations[peak_concentration_index]), 0)
    percentil_05 = round(np.quantile(
        concentrations[peak_concentration_index], 0.05), 0)
    percentil_95 = round(np.quantile(
        concentrations[peak_concentration_index], 0.95), 0)
    print('Peak concentration: ', f'{mean} [{percentil_05} - {percentil_95}]')

    ############ Plot the results #############
    plt.plot(times, [np.mean(c) for c in concentrations])
    # plt.fill_between(times, [np.quantile(c, 0.05) for c in concentrations], [
                    #  np.quantile(c, 0.95) for c in concentrations], alpha=0.2)
    plt.xlabel('Time of day')
    plt.ylabel('concentration (IRP / m3)')
    plt.show()

    ############# Inhaled dose ############
    deposited_exposure: models._VectorisedFloat = model.deposited_exposure()
    mean = round(np.mean(deposited_exposure), 0)
    percentil_05 = round(np.quantile(deposited_exposure, 0.05), 0)
    percentil_95 = round(np.quantile(deposited_exposure, 0.95), 0)
    print('Inhaled dose: ', f'{mean} [{percentil_05} - {percentil_95}]')

    ############# Plot the results #############
    # cumulative_doses: models._VectorisedFloat = np.cumsum([
    #     np.array(model.deposited_exposure_between_bounds(
    #         float(time1), float(time2))).mean()
    #     for time1, time2 in zip(times[:-1], times[1:])
    # ])
    # cumulative_05: models._VectorisedFloat = np.cumsum([
    #     np.quantile(np.array(model.deposited_exposure_between_bounds(
    #         float(time1), float(time2))), 0.05)
    #     for time1, time2 in zip(times[:-1], times[1:])
    # ])
    # cumulative_95: models._VectorisedFloat = np.cumsum([
    #     np.quantile(np.array(model.deposited_exposure_between_bounds(
    #         float(time1), float(time2))), 0.95)
    #     for time1, time2 in zip(times[:-1], times[1:])
    # ])
    # plt.plot(times[:-1], cumulative_doses)
    # plt.fill_between(times[:-1], cumulative_05, cumulative_95, alpha=0.2)
    # plt.show()

    ############# Peak CO2 concentration #############
    if CO2_values:
        # For the CO2 concentration profile we assume that the breaks between the infected and exposed are similar.
        CO2_model: models.CO2ConcentrationModel = models.CO2ConcentrationModel(
            data_registry=data_registry,
            room=model.concentration_model.room,
            ventilation=model.concentration_model.ventilation,
            CO2_emitters=model.concentration_model.infected,
        )

        concentrations: models._VectorisedFloat = [
            np.array(CO2_model.concentration(float(time)))
            for time in times
        ]

        dynamic_exposed: models.Population = model.exposed
        CO2_model_exposed: models.CO2ConcentrationModel = dataclass_utils.nested_replace(CO2_model, {
            'CO2_emitters.number': dynamic_exposed.number,
            'CO2_emitters.presence': dynamic_exposed.presence,
            'CO2_emitters.activity': dynamic_exposed.activity,
        })

        # Sum contributions from both populations, and the background.
        concentrations: models._VectorisedFloat = np.add(concentrations, [
            np.array(CO2_model_exposed.concentration(float(time)))
            for time in times
        ]) + CO2_model_exposed.min_background_concentration()

        peak_concentration_index: int = np.argmax(
            [c.mean() for c in concentrations])

        mean = round(np.mean(concentrations[peak_concentration_index]), 0)
        percentil_05 = round(np.quantile(
            concentrations[peak_concentration_index], 0.05), 0)
        percentil_95 = round(np.quantile(
            concentrations[peak_concentration_index], 0.95), 0)
        print('Peak concentration: ',
              f'{mean} [{percentil_05} - {percentil_95}]')

        ############# Plot the results #############
        plt.plot(times, [np.mean(c) for c in concentrations])
        plt.fill_between(times, [np.quantile(c, 0.05) for c in concentrations], [
                         np.quantile(c, 0.95) for c in concentrations], alpha=0.2)
        plt.show()

    return


def generate_co2_curve(concentration_model: models.CO2ConcentrationModel) -> None:
    # Plot the collected data
    day_times: list = [8.0, 8.033333333333333, 8.066666666666666, 8.1, 8.133333333333333, 8.166666666666666, 8.2, 8.233333333333333, 8.266666666666667, 8.3, 8.333333333333334, 8.366666666666667, 8.4, 8.433333333333334, 8.466666666666667, 8.5, 8.533333333333333, 8.566666666666666, 8.6, 8.633333333333333, 8.666666666666666, 8.7, 8.733333333333333, 8.766666666666667, 8.8, 8.833333333333334, 8.866666666666667, 8.9, 8.933333333333334, 8.966666666666667, 9.0, 9.033333333333333, 9.066666666666666, 9.1, 9.133333333333333, 9.166666666666666, 9.2, 9.233333333333333, 9.266666666666667, 9.3, 9.333333333333334, 9.366666666666667, 9.4, 9.433333333333334, 9.466666666666667, 9.5, 9.533333333333333, 9.566666666666666, 9.6, 9.633333333333333, 9.666666666666666, 9.7, 9.733333333333333, 9.766666666666667, 9.8, 9.833333333333334, 9.866666666666667, 9.9, 9.933333333333334, 9.966666666666667, 10.0, 10.033333333333333, 10.066666666666666, 10.1, 10.133333333333333, 10.166666666666666, 10.2, 10.233333333333333, 10.266666666666667, 10.3, 10.333333333333334, 10.366666666666667, 10.4, 10.433333333333334, 10.466666666666667, 10.5, 10.533333333333333, 10.566666666666666, 10.6, 10.633333333333333, 10.666666666666666, 10.7, 10.733333333333333, 10.766666666666667, 10.8, 10.833333333333334, 10.866666666666667, 10.9, 10.933333333333334, 10.966666666666667, 11.0, 11.033333333333333, 11.066666666666666, 11.1, 11.133333333333333, 11.166666666666666, 11.2, 11.233333333333333, 11.266666666666667, 11.3, 11.333333333333334, 11.366666666666667, 11.4, 11.433333333333334, 11.466666666666667, 11.5, 11.533333333333333, 11.566666666666666, 11.6, 11.633333333333333, 11.666666666666666, 11.7, 11.733333333333333, 11.766666666666667, 11.8, 11.833333333333334, 11.866666666666667, 11.9, 11.933333333333334, 11.966666666666667, 12.0, 12.033333333333333, 12.066666666666666, 12.1, 12.133333333333333, 12.166666666666666, 12.2, 12.233333333333333, 12.26666666666667, 12.3, 12.333333333333336, 12.366666666666667, 12.400000000000002, 12.433333333333334, 12.466666666666669, 12.5, 12.533333333333335, 12.566666666666666, 12.600000000000001, 12.633333333333333, 12.666666666666668, 12.7, 12.733333333333334, 12.766666666666666, 12.8, 12.833333333333332, 12.866666666666667, 12.899999999999999, 12.933333333333334, 12.966666666666665, 13.0, 13.033333333333331, 13.066666666666666, 13.099999999999998, 13.133333333333333, 13.166666666666664, 13.2, 13.23333333333333, 13.266666666666667, 13.3, 13.333333333333334, 13.366666666666667, 13.4, 13.433333333333334, 13.466666666666667, 13.5, 13.533333333333333, 13.566666666666666, 13.6, 13.633333333333333, 13.666666666666666, 13.7, 13.733333333333333, 13.76666666666667, 13.8, 13.833333333333336, 13.866666666666667, 13.900000000000002, 13.933333333333334, 13.966666666666669, 14.0, 14.033333333333335,
                       14.066666666666666, 14.100000000000001, 14.133333333333333, 14.166666666666668, 14.2, 14.233333333333334, 14.266666666666666, 14.3, 14.333333333333332, 14.366666666666667, 14.399999999999999, 14.433333333333334, 14.466666666666665, 14.5, 14.533333333333331, 14.566666666666666, 14.599999999999998, 14.633333333333333, 14.666666666666664, 14.7, 14.73333333333333, 14.766666666666667, 14.8, 14.833333333333334, 14.866666666666667, 14.9, 14.933333333333334, 14.966666666666667, 15.0, 15.033333333333333, 15.066666666666666, 15.1, 15.133333333333333, 15.166666666666666, 15.2, 15.233333333333333, 15.26666666666667, 15.3, 15.333333333333336, 15.366666666666667, 15.400000000000002, 15.433333333333334, 15.466666666666669, 15.5, 15.533333333333335, 15.566666666666666, 15.600000000000001, 15.633333333333333, 15.666666666666668, 15.7, 15.733333333333334, 15.766666666666666, 15.8, 15.833333333333332, 15.866666666666667, 15.899999999999999, 15.933333333333334, 15.966666666666665, 16.0, 16.033333333333335, 16.066666666666666, 16.1, 16.133333333333333, 16.166666666666668, 16.2, 16.233333333333334, 16.266666666666666, 16.3, 16.333333333333332, 16.366666666666667, 16.4, 16.433333333333334, 16.466666666666665, 16.5, 16.533333333333335, 16.566666666666666, 16.6, 16.633333333333333, 16.666666666666668, 16.7, 16.733333333333334, 16.766666666666666, 16.8, 16.833333333333332, 16.866666666666667, 16.9, 16.933333333333334, 16.966666666666665, 17.0, 17.033333333333335, 17.066666666666666, 17.1, 17.133333333333333, 17.166666666666668, 17.2, 17.233333333333334, 17.266666666666666, 17.3, 17.333333333333332, 17.366666666666667, 17.4, 17.433333333333334, 17.466666666666665, 17.5, 17.533333333333335, 17.566666666666666, 17.6, 17.633333333333333, 17.666666666666668, 17.7, 17.733333333333334, 17.766666666666666, 17.8, 17.833333333333332, 17.866666666666667, 17.9, 17.933333333333334, 17.966666666666665, 18.0, 18.033333333333335, 18.066666666666666, 18.1, 18.133333333333333, 18.166666666666668, 18.2, 18.233333333333334, 18.266666666666666, 18.3, 18.333333333333332, 18.366666666666667, 18.4, 18.433333333333334, 18.466666666666665, 18.5, 18.533333333333335, 18.566666666666666, 18.6, 18.633333333333333, 18.666666666666668, 18.7, 18.733333333333334, 18.766666666666666, 18.8, 18.833333333333332, 18.866666666666667, 18.9, 18.933333333333334, 18.966666666666665, 19.0, 19.033333333333335, 19.066666666666666, 19.1, 19.133333333333333, 19.166666666666668, 19.2, 19.233333333333334, 19.266666666666666, 19.3, 19.333333333333332, 19.366666666666667, 19.4, 19.433333333333334, 19.466666666666665, 19.5, 19.533333333333335, 19.566666666666666, 19.6, 19.633333333333333, 19.666666666666668, 19.7, 19.733333333333334, 19.766666666666666, 19.8, 19.833333333333332, 19.866666666666667, 19.9, 19.933333333333334, 19.966666666666665]
    day_co2: list = [445.189166666667, 443.284166666667, 440.908333333333, 443.430833333333, 442.365833333333, 444.094166666667, 445.151666666667, 445.655833333333, 447.9675, 447.998333333333, 443.95, 442.546666666667, 439.313333333333, 438.225, 441.4325, 441.19, 443.804166666667, 445.173333333333, 446.494166666667, 445.2775, 452.073333333333, 458.844166666667, 470.828333333333, 478.146666666667, 488.3375, 502.125833333333, 522.056666666667, 545.519166666667, 579.880833333333, 616.245, 641.154166666667, 676.288333333333, 701.9375, 720.464166666667, 746.933333333333, 765.83, 779.098333333333, 794.173333333333, 810.624166666667, 825.966666666667, 838.34, 854.355, 876.381666666667, 886.208333333334, 898.408333333333, 921.7175, 942.848333333333, 953.811666666667, 978.955833333333, 990.320833333333, 1002.93083333333, 1017.36083333333, 1029.37916666667, 1041.02833333333, 1051.8825, 1067.22, 1073.53, 1079.73833333333, 1093.73333333333, 1104.81416666667, 1125.7975, 1141.115, 1151.04583333333, 1160.0525, 1176.36666666667, 1193.665, 1180.10416666667, 1015.33416666667, 864.745833333333, 802.680833333333, 774.455, 728.268333333333, 697.325833333333, 676.063333333333, 657.555, 640.564166666667, 606.534166666667, 595.925, 577.7525, 553.605, 530.2125, 524.968333333333, 523.1525, 521.534166666667, 512.944166666667, 505.296666666667, 502.055833333333, 502.463333333333, 505.2475, 507.476666666667, 509.170833333333, 511.3125, 513.78, 520.3925, 529.136666666667, 532.798333333333, 530.110833333333, 523.964166666667, 521.574166666667, 519.051666666667, 510.294166666667, 509.981666666667, 514.349166666667, 518.395833333333, 524.6025, 521.003333333333, 519.448333333333, 523.3125, 527.46, 528.325833333333, 526.355, 527.008333333333, 529.9675, 534.019166666667, 535.615833333333, 533.514166666667, 530.551666666667, 522.348333333333, 524.2425, 532.020833333333, 539.126666666667, 538.835833333333, 526.185833333333, 517.509166666667, 507.993333333333, 493.7025, 485.631666666667, 479.526666666667, 471.584166666667, 472.225833333333, 468.205833333333, 463.099166666667, 461.0375, 458.98, 456.354166666667, 458.615, 459.161666666667, 462.9625, 465.558333333333, 468.448333333333, 475.206666666667, 480.3225, 488.961666666667, 527.991818181818, 579.613333333333, 606.594166666667, 611.2175, 617.0225, 635.926666666667, 651.079166666667, 676.646666666667, 696.63, 714.603333333333, 729.926666666667, 744.6525, 765.995833333333, 788.4925, 812.105833333333, 832.75, 854.715, 883.851666666667, 895.591666666667, 910.026666666667, 924.373333333333, 944.516666666667, 956.769166666667, 971.446666666667, 981.2725, 993.645833333333, 1004.37833333333, 1021.56833333333, 1035.155, 1043.84916666667, 1063.7225, 1070.96083333333, 1065.62416666667, 1065.89333333333, 1073.72333333333, 1086.39333333333,
                     1093.525, 1120.085, 1189.26, 1202.875, 1218.55583333333, 1238.46416666667, 1250.06, 1263.46, 1265.04333333333, 1270.10166666667, 1281.61, 1294.92416666667, 1304.21833333333, 1315.50583333333, 1338.43416666667, 1351.53083333333, 1353.35916666667, 1364.0425, 1361.66583333333, 1343.3225, 1329.69833333333, 1320.43583333333, 1310.45, 1313.62166666667, 1305.505, 1313.36, 1307.45916666667, 1289.97666666667, 1286.87666666667, 1289.315, 1276.8075, 1268.87083333333, 1266.07083333333, 1264.0475, 1271.76416666667, 1268.46833333333, 1244.53166666667, 1206.35416666667, 1173.62666666667, 1144.95833333333, 1157.15166666667, 1194.38333333333, 1198.275, 1196.0825, 1182.46583333333, 1167.85666666667, 1150.36083333333, 1132.83833333333, 1108.08, 1097.41583333333, 1099.82333333333, 1093.3775, 1086.7575, 1086.875, 1083.80166666667, 1075.48166666667, 1059.92083333333, 1048.40416666667, 1047.35, 1042.55166666667, 1036.11333333333, 1026.88333333333, 1022.6775, 1017.64666666667, 1023.52083333333, 1021.01666666667, 1017.255, 1004.57166666667, 908.28, 906.460833333333, 979.216666666667, 955.8475, 928.884166666667, 915.265833333333, 914.135833333333, 930.121666666666, 923.345833333333, 920.959166666667, 865.924166666667, 860.181666666667, 867.003333333333, 869.708333333333, 871.378333333333, 861.466666666667, 862.920833333334, 850.374166666667, 843.933333333333, 839.650833333334, 838.1425, 839.793333333333, 849.7425, 841.635833333333, 820.793333333333, 824.950833333333, 838.52, 853.844166666667, 855.645, 838.704166666667, 818.070833333333, 811.7225, 804.270833333333, 794.473333333333, 790.601666666667, 781.965, 788.555833333333, 779.635833333333, 804.179166666667, 836.930833333333, 852.391666666667, 856.87, 858.073333333333, 857.895, 856.51, 856.744166666667, 851.720833333333, 849.561666666667, 849.17, 846.0125, 846.755833333333, 844.778333333333, 841.989166666667, 838.9925, 836.0375, 833.210833333333, 832.631666666667, 830.8925, 825.924166666667, 823.550833333333, 823.720833333333, 818.218333333333, 812.421666666667, 810.169166666667, 808.388333333333, 806.663333333333, 803.0125, 800.164166666667, 794.33, 790.45, 790.359166666667, 787.696666666667, 783.316666666667, 780.483333333334, 783.9625, 780.8675, 780.761666666667, 777.228333333333, 774.976666666667, 768.6975, 763.635, 761.453333333334, 757.805, 760.021666666667, 762.135833333333, 761.649166666667, 761.2175, 761.484166666667, 757.53, 754.458333333333, 752.550833333333, 752.123333333333, 751.679166666666, 748.565, 744.345833333333, 742.103333333333, 737.648333333333, 731.049166666667, 732.635833333333, 726.8275, 726.21, 726.948333333333, 727.14, 726.7575, 728.934166666667, 729.886666666667, 726.511666666667, 724.84, 723.9075, 722.995833333333, 721.1125, 720.204166666667, 721.064166666667]

    # Generate the monte carlo CO2ConcentrationModel and plot result
    mc_model: models.ConcentrationModel = concentration_model.build_model(
        SAMPLE_SIZE)
    times: typing.List[float] = interesting_times(concentration_model)
    times.append(day_times[-1])  # Extend the curve to the last hour
    concentrations: models._VectorisedFloat = [
        np.array(mc_model.concentration(float(time))).mean()
        for time in times
    ]

    plt.plot(list(times), concentrations, lw=3, color='salmon',
             label="Predicted model (CAiMIRA)", zorder=1)

    for i, (presence_start, presence_finish) in enumerate(concentration_model.CO2_emitters.presence.boundaries()):
        plt.fill_between(
            times, concentrations, 0,
            where=(np.array(times) >= presence_start) & (
                np.array(times) <= presence_finish),
            color='blue', alpha=0.05,
            label="Occupant(s) present" if i == 0 else "",
        )

    plt.scatter(day_times, day_co2, marker='.', s=15,
                color='k', label="Sensor data", zorder=2)
    plt.xticks(plt.xticks()[0], np.array(
        [str(int(x)) + ':' + "{:02d}".format(int((x*60) % 60)) for x in plt.xticks()[0]]))

    plt.legend()
    plt.ylim(400, 1800)
    plt.xlabel('Time of day')
    plt.ylabel('CO$_2$ concentration (ppm)')
    plt.show()

    return None


def generate_comparison_sr_lr_curve(exposure_model: models.ExposureModel,
                                    expirations: list,
                                    max_distance: float,
                                    save_png: bool,
                                    with_mean_aerosols: bool =False) -> None:
    
    distances: list = np.linspace(0, max_distance, 50)
    factors = comparison_sr_lr_threshold(exposure_model=exposure_model,
                                         expirations=expirations, 
                                         max_distance=max_distance,
                                         with_mean_aerosols=with_mean_aerosols)

    for exp_act_fac in factors:
        if exp_act_fac['expiration'] == "Breathing":
            color = 'C0'
        if exp_act_fac['expiration'] == "Speaking":
            color = 'C1'
        if exp_act_fac['expiration'] == "Shouting":
            color = 'C2'

        if exp_act_fac['activity'] == 'Seated':
            ls = 'dashed'
        if exp_act_fac['activity'] == 'Light activity':
            ls = 'solid'
        if exp_act_fac['activity'] == 'Moderate activity':
            ls = 'dotted'

        plt.plot(distances, exp_act_fac['factors'], color=color, linestyle=ls)

    # customized legend
    handles, labels = plt.gca().get_legend_handles_labels()
    dashed_line = Line2D(
        [0], [0], label='Sedentary/Seated', color='k', ls='dashed')
    solid_line = Line2D([0], [0], label='Light activity',
                        color='k', ls='solid')
    dotted_line = Line2D(
        [0], [0], label='Moderate activity', color='k', ls='dotted')
    if 'Breathing' in expirations:
        plt.text(.3, 0.9, '$\it{Breathing}$',
                 color='C0', fontsize=12, weight='bold')
    if 'Speaking' in expirations:
        plt.text(2.5, 0.5, '$\it{Speaking}$', color='C1',
                 fontsize=12, weight='bold')
    handles.extend([dashed_line, solid_line, dotted_line])
    plt.legend(handles=handles)

    # plt.yscale('log')
    # plt.hlines(0.97, distances[0], distances[-1], ls='--')
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.xlabel('Distance (m)', fontsize=13)
    # y-axis: Steady state long-range concentration\nnormalized by the concentration at short-range
    plt.ylabel('N$_{LR/SR}$', fontsize=13)

    if save_png:
        plt.savefig('comparisson_lr_sr.png', dpi=300)
    plt.show()

    return None


def comparison_sr_lr_threshold(exposure_model: models.ExposureModel, expirations: list, max_distance: float, with_mean_aerosols: bool = False) -> models._VectorisedFloat:
    distances: list = np.linspace(0, max_distance, 50)
    room_humidity: float = exposure_model.concentration_model.room.humidity
    room_volume: float = exposure_model.concentration_model.room.volume

    factors_with_expiration_and_activity: models._VectorisedFloat = []

    for expiration in expirations:
        SAMPLE_SIZE = 250_000 if expiration == "Breathing" else 1_000_000

        # Exposure model
        mc_model: models.ExposureModel = exposure_model.build_model(SAMPLE_SIZE)

        # Expiration distribution
        expiration_dist: models.Expiration = expiration_distributions(data_registry)[expiration].build_model(SAMPLE_SIZE)

        # Short-range expiration distribution
        sr_expiration_dist: models.Expiration = short_range_expiration_distributions(data_registry)[expiration].build_model(SAMPLE_SIZE)

        # Viral load distribution
        viral_load: models._VectorisedFloat = mc_model.concentration_model.infected.virus.viral_load_in_sputum

        # Initilization of SR model:
        short_range: SimpleShortRangeModel = short_range_model(
            long_range_concentration=0.,  # Not relevant
            expiration=sr_expiration_dist,  # Not relevant
            activity=mc_model.concentration_model.infected.activity,  # Not relevant
            presence=models.SpecificInterval(
                ((8, 12),)),  # Not relevant
            distance=0.,  # Not relevant
            with_mean_aerosols=with_mean_aerosols,
        )
        for activity in ('Seated', 'Light activity', 'Moderate activity'):

            activity_dist: models._VectorisedFloat = activity_distributions(data_registry)[activity].build_model(
                SAMPLE_SIZE)
            mc_model_expiration_activity: models.ExposureModel = dataclass_utils.nested_replace(mc_model, {
                'concentration_model.infected.expiration': expiration_dist,
                'concentration_model.infected.activity': activity_dist,
            })

            if with_mean_aerosols:
                # np.mean(aerosols) correspond to the MC integration of the particle diameters. 
                # In order to return the results in IRP / m3 (instead of IRP / (m3.µm))
                aerosols: models._VectorisedFloat = mc_model_expiration_activity.concentration_model.infected.expiration.aerosols(models.Mask.types['No mask'])
                emission_rate: models._VectorisedFloat = mc_model_expiration_activity.concentration_model.infected.emission_rate_per_person_when_present() / aerosols * np.mean(aerosols)
            else:
                # without np.mean(aerosols) this returns IRP / (m3.µm)
                emission_rate: models._VectorisedFloat = mc_model_expiration_activity.concentration_model.infected.emission_rate_per_person_when_present()
            
            steady_state: models._VectorisedFloat = (
                (emission_rate * mc_model_expiration_activity.concentration_model.infected.number) /
                # given there is no natural ventilation, removal rate is totally time independent.
                (mc_model_expiration_activity.concentration_model.removal_rate(
                    time=0) * room_volume)
            )

            factors: list = []
            for d in tqdm(distances):
                short_range_d: SimpleShortRangeModel = dataclass_utils.nested_replace(
                    short_range, {
                        'long_range_concentration': steady_state,
                        'activity': activity_dist,
                        'expiration': sr_expiration_dist,
                        'distance': d,
                    }
                )

                short_range_concentration = short_range_d.concentration(
                    viral_load, room_humidity, room_humidity)
                factors.append(np.mean(steady_state) /
                               np.mean(short_range_concentration))

            print(
                'Expiration: ', expiration,
                '// Activity: ', activity,
                '// Distance (LR/SR >= 0.97): ', round(
                    distances[next(x[0] for x in enumerate(factors) if x[1] > 0.97)], 1), 'm'
            )
            
            factors_with_expiration_and_activity.append({
                'activity': activity,
                'expiration': expiration,
                'factors': factors,
            })

    return factors_with_expiration_and_activity


def model_dose_results(exposure_models: typing.List[models.ExposureModel], labels: typing.List[str]):
    for model, label in zip(exposure_models, labels):
        mc_model: models.ExposureModel = model.build_model(size=SAMPLE_SIZE)
        times: models._VectorisedFloat = rep_gen.interesting_times(mc_model, 500)

        if model.short_range != ():
            cumulative_doses: models._VectorisedFloat = np.cumsum([
                np.array(mc_model.deposited_exposure_between_bounds(
                    float(time1), float(time2))).mean()
                for time1, time2 in zip(times[:-1], times[1:])
            ])
            plt.plot(times[1:], cumulative_doses,
                     label=f'{label}: With short range interactions')
            # print(
            #     'Scenario: ', label,
            #     '\n// Dose short-range: ', cumulative_doses[-1],
            #     '\n>>>>>>>>>>>>',
            # )

            for index, sr in enumerate(model.short_range):
                start = sr.presence.present_times[0][0]
                stop = sr.presence.present_times[0][-1]
                index_t_at_start = next(x[0] for x in enumerate(
                    times[1:]) if x[1] >= start) - 1
                index_t_at_stop = next(
                    x[0] for x in enumerate(times[1:]) if x[1] >= stop)
                
                decimal_time = round(times[1:][index_t_at_start], 2)
                hours = int(decimal_time)  # Integer part (hours)
                minutes = int((decimal_time - hours) * 60)  # Decimal part converted to minutes
                index_t_at_start_time_formatted = f"{hours}h{minutes}"

                decimal_time = round(times[1:][index_t_at_stop], 2)
                hours = int(decimal_time)  # Integer part (hours)
                minutes = int((decimal_time - hours) * 60)  # Decimal part converted to minutes
                index_t_at_stop_time_formatted = f"{hours}h{minutes}"

                print(
                    'Scenario: ', label,
                    # f'\n// Short-range interaction no. {index}',
                    f'\n// Initial dose (t = {index_t_at_start_time_formatted}) = {round(cumulative_doses[index_t_at_start], 2)}',
                    f'\n// Final dose (t = {index_t_at_stop_time_formatted}) = {round(cumulative_doses[index_t_at_stop], 2)}',
                    f'\n// Dose difference (short-range contribution): {round(cumulative_doses[index_t_at_stop] - cumulative_doses[index_t_at_start], 2)}'
                    '\n>>>>>>>>>>>>',
                )

        model_without_sr: models.ExposureModel = dataclass_utils.nested_replace(
            mc_model, {'short_range': ()})
        cumulative_doses: models._VectorisedFloat = np.cumsum([
            np.array(model_without_sr.deposited_exposure_between_bounds(
                float(time1), float(time2))).mean()
            for time1, time2 in zip(times[:-1], times[1:])
        ])
        plt.plot(times[1:], cumulative_doses,
                 label=f'{label}: Without short range interactions')
        print(
            'Scenario: ', label,
            '\n// Dose long-range: ', cumulative_doses[-1],
            '\n>>>>>>>>>>>>',
        )

    plt.legend()
    plt.xlabel('Time of day', fontsize=13)
    plt.ylabel('Mean cumulative dose (infectious virus)', fontsize=13)
    plt.show()


def model_deterministic_exposure(exposure_models: typing.List[models.ExposureModel], labels: typing.List[str]):
    print('\n<<<<<<<<<<< Deterministic exposure values for different scenarios >>>>>>>>>>>')
    pis, q_05, q_95 = [], [], []
    for model in exposure_models:
        pi: models._VectorisedFloat = np.array(model.build_model(
            SAMPLE_SIZE).infection_probability()/100)
        pis.append(np.mean(pi))
        q_05.append(np.quantile(pi, 0.05))
        q_95.append(np.quantile(pi, 0.95))

    for index, label in enumerate(labels):
        print('Scenario: ', label)
        print('Deterministic Exposure P(I): ', pis[index])
        print('Quantile 5: ', q_05[index])
        print('Quantile 95: ', q_95[index])
        print('\n')


def model_probabilistic_exposure(exposure_models: typing.List[models.ExposureModel], labels: typing.List[str], cases: models.Cases, show_deterministic_prob: bool):
    print('\n<<<<<<<<<<< Probabilistic exposure values for different scenarios >>>>>>>>>>>')
    if show_deterministic_prob:
        pis = []

    total_prob_rules, q_05, q_95 = [], [], []
    for model in exposure_models:
        mc_model: models.ExposureModel = model.build_model(SAMPLE_SIZE)
        if show_deterministic_prob:
            pis.append(np.mean(mc_model.infection_probability()/100))

        geographic_cases_model = dataclass_utils.nested_replace(
            mc_model, {'geographical_data': cases})

        total_prob: models._VectorisedFloat = np.array(
            geographic_cases_model.total_probability_rule()/100)
        total_prob_rules.append(np.mean(total_prob))
        q_05.append(np.quantile(total_prob, 0.05))
        q_95.append(np.quantile(total_prob, 0.95))

    for index, label in enumerate(labels):
        print('Scenario: ', label)
        if show_deterministic_prob:
            print('Deterministic Exposure P(I): ', pis[index])
        print('Probabilistic Exposure: ', total_prob_rules[index])
        print('Quantile 5: ', q_05[index])
        print('Quantile 95: ', q_95[index])
        print('\n')


def compare_vl_hist() -> None:
    # Symptomatic viral load data
    sympt_vl: models._VectorisedFloat = symptomatic_vl_frequencies(data_registry).generate_samples(SAMPLE_SIZE)
    viral_loads: models._VectorisedFloat = [np.log10(vl) for vl in sympt_vl]
    plt.hist(viral_loads, bins=300, color='lightgrey', label='Jacot et al. (2020)', density=True)

    # Covid overal viral load data
    covid_overal_vl: models._VectorisedFloat = covid_overal_vl_data(data_registry).generate_samples(SAMPLE_SIZE)
    viral_loads: models._VectorisedFloat = [np.log10(vl) for vl in covid_overal_vl]
    plt.hist(viral_loads, bins=300, color='lightsteelblue', label='Chen et al. (2021)', alpha=0.7, density=True)

    plt.xlabel('vl$_{\mathrm{in}}$ (log$_{10}$ RNA copies mL$^{-1}$)')
    plt.ylabel('Density')

    plt.legend()
    plt.show()

    return None


def statistics_for_random_variable(physical_activity: bool = False, 
                                   viral_load: bool = False, 
                                   mask: bool = False, 
                                   viable_to_RNA: bool = False,
                                   infectious_dose: bool = False,
                                   interpersonal_distance: bool = False) -> None:
    print('\n<<<<<<<<<<< Random (stochastic) variables >>>>>>>>>>>')
    if (physical_activity):
        print('\n<<<<<<<<<<< Breathing flowrate >>>>>>>>>>>')
        for activity in ('Seated', 'Standing', 'Light activity', 'Moderate activity', 'Heavy exercise'):
            activity_dist: models.Activity = activity_distributions(data_registry)[activity].build_model(SAMPLE_SIZE)
            breathing_rate: models._VectorisedFloat = activity_dist.exhalation_rate
            print(f'\n<<<<< {activity} <<<<<')
            print('Mean: ', np.round(np.mean(breathing_rate), 3))
            print('SD: ', np.round(np.std(breathing_rate), 3))

    if viral_load:
        print('\n<<<<<<<<<<< Viral load >>>>>>>>>>>')
        viral_load_in_sputum: models._VectorisedFloat = [np.log10(vl) for vl in covid_overal_vl_data(data_registry).generate_samples(SAMPLE_SIZE)]
        print('Mean: ', np.round(np.mean(viral_load_in_sputum), 1))
        print('SD: ', np.round(np.std(viral_load_in_sputum), 1))

    if mask:
        print('\n<<<<<<<<<<< Mask efficiency >>>>>>>>>>>')
        for mask in ('Type I', 'FFP2', 'Cloth'):
            mask_dist: models.Mask = mask_distributions(data_registry)[mask].build_model(SAMPLE_SIZE)
            η_inhale: models._VectorisedFloat = mask_dist.η_inhale
            print(f'\n<<<<< {mask} <<<<<')
            print('[', np.round(min(η_inhale), 3), ' - ', np.round(max(η_inhale), 3), ']')
    
    if viable_to_RNA:
        print('\n<<<<<<<<<<< viable-to-RNA virus ratio >>>>>>>>>>>')
        viable_to_RNA: models._VectorisedFloat = viable_to_RNA_ratio_distribution().generate_samples(SAMPLE_SIZE)
        print('[', np.round(min(viable_to_RNA), 3), ' - ', np.round(max(viable_to_RNA), 3), ']')

    if infectious_dose:
        print('\n<<<<<<<<<<< Infectious dose >>>>>>>>>>>')
        infectious_dose: models._VectorisedFloat = infectious_dose_distribution().generate_samples(SAMPLE_SIZE)
        print('[', np.round(min(infectious_dose), 3), ' - ', np.round(max(infectious_dose), 3), ']')

    if interpersonal_distance:
        print('\n<<<<<<<<<<< Interpersonal distance >>>>>>>>>>>')
        sr_distances: models._VectorisedFloat = short_range_distances(data_registry).generate_samples(SAMPLE_SIZE)
        print('Mean: ', np.round(np.mean(sr_distances), 3))
        print('SD: ', np.round(np.std(sr_distances), 3))
    
    print('\n')
    return None


def short_range_emission_rate_values(physical_activity: str, 
                                     expiratory_activity: typing.Union[str, dict],
                                     virus_dist: typing.Optional[models.Virus] = None,
                                     with_mean_aerosols: bool = False,
                                     print_results: bool = True,
                                     use_f_inf_beta_dist: bool = False):
    
    if virus_dist:
        SAMPLE_SIZE = len(virus_dist.viral_load_in_sputum)
    else:
        SAMPLE_SIZE = 250_000 if expiratory_activity == "Breathing" else 1_000_000

    # Activity distribution
    activity_dist: models.Activity = activity_distributions(data_registry)[physical_activity].build_model(SAMPLE_SIZE)

    # Short-range expiration distribution
    if isinstance(expiratory_activity, str):
        short_range_expiration_dist: models.Expiration = short_range_expiration_distributions(data_registry)[expiratory_activity].build_model(SAMPLE_SIZE)
    elif isinstance(expiratory_activity, dict):
        short_range_expiration_dist: models.Expiration = short_range_build_expiration(data_registry, expiratory_activity).build_model(SAMPLE_SIZE)

    # Short-range distances distribution
    short_range_distances_dist: models._VectorisedFloat = short_range_distances(data_registry).generate_samples(SAMPLE_SIZE)

    # Virus distribution
    if not virus_dist:
        initial_virus_dist: models.Virus = virus_distributions(data_registry)['SARS_CoV_2_OMICRON'].build_model(SAMPLE_SIZE)
        if use_f_inf_beta_dist:
            f_inf = f_inf_beta_distribution(SAMPLE_SIZE=SAMPLE_SIZE, render_graph=False) # alpha = 2, beta = 5, lower = 10**-4, upper = 10**-2
            virus_dist = dataclass_utils.nested_replace(
                initial_virus_dist, {
                    'viable_to_RNA_ratio': f_inf
                }
            )
        else:
            virus_dist = initial_virus_dist

    # Infected population
    infected: models.InfectedPopulation = models.InfectedPopulation(
        data_registry=data_registry,
        number=1, # not relevant
        presence=models.SpecificInterval(((8, 12),)), # not relevant
        activity=activity_dist,
        mask=models.Mask.types['No mask'],
        host_immunity=0,
        virus=virus_dist,
        expiration=[0]*SAMPLE_SIZE, # not relevant for the short-range emission rate
    )
    short_range_model: models.ShortRangeModel = models.ShortRangeModel(
        data_registry=data_registry,
        expiration=short_range_expiration_dist,
        activity=activity_dist,
        presence=models.SpecificInterval(((8, 12),)), # not relevant
        distance=short_range_distances_dist,
    )
    
    jet_origin_concentration: models._VectorisedFloat = short_range_model.jet_origin_concentration(infected) # IRP/m^3
    if with_mean_aerosols:
        # np.mean(aerosols) correspond to the MC integration of the particle diameters. 
        # In order to return the results in IRP / m3 (instead of IRP / (m3.µm))
        aerosols: models._VectorisedFloat = short_range_expiration_dist.aerosols(models.Mask.types['No mask'])
        joc: models._VectorisedFloat = jet_origin_concentration / aerosols * np.mean(aerosols)
    else:
        # without np.mean(aerosols) this returns IRP / (m3.µm)
        joc: models._VectorisedFloat = jet_origin_concentration
        
    breathing_rate: models._VectorisedFloat = activity_dist.exhalation_rate # m^3/h
    # The emission rate at short-range is derived by multiplying the jet origin concentration with the breathing rate
    emission_rate: models._VectorisedFloat = joc * breathing_rate # IRP/h
    log10_emission_rate: models._VectorisedFloat = [np.log10(er) for er in emission_rate]
    
    if print_results:
        print('\n<<<<<<<<<<< ' + str(expiratory_activity) + ' model statistics >>>>>>>>>>>')
        print_er_info(emission_rate, log10_emission_rate)

    return emission_rate, log10_emission_rate

def present_vl_er_histograms(physical_activity: str, 
                             expiratory_activities: typing.List, 
                             with_mean_aerosols: bool = False, 
                             use_f_inf_beta_dist: bool = False) -> None:
    # Virus distribution
    initial_virus_dist: models.Virus = virus_distributions(data_registry)['SARS_CoV_2_OMICRON'].build_model(SAMPLE_SIZE)
    if use_f_inf_beta_dist:
        f_inf = f_inf_beta_distribution(SAMPLE_SIZE=SAMPLE_SIZE, render_graph=False) # alpha = 2, beta = 5, lower = 10**-4, upper = 10**-2
        virus_dist = dataclass_utils.nested_replace(
            initial_virus_dist, {
                'viable_to_RNA_ratio': f_inf
            }
        )
    else:
        virus_dist = initial_virus_dist

    emission_rate_dict: typing.Dict = {}
    log10_emission_rate_dict: typing.Dict = {}
    for exp_act in expiratory_activities:
        emission_rate, log10_emission_rate = short_range_emission_rate_values(physical_activity=physical_activity, 
                                                                              expiratory_activity=exp_act, 
                                                                              virus_dist=virus_dist,
                                                                              with_mean_aerosols=with_mean_aerosols,
                                                                              print_results=False)
        emission_rate_dict[exp_act] = emission_rate
        log10_emission_rate_dict[exp_act] = log10_emission_rate

    # Viral load
    viral_loads = models._VectorisedFloat = [np.log10(vl) for vl in virus_dist.viral_load_in_sputum]

    # Plot material
    fig, axs = plt.subplots(1, 2, sharex=False, sharey=False)
    fig.set_figheight(5)
    fig.set_figwidth(10)
    plt.tight_layout()
    plt.subplots_adjust(wspace=0.2)
    plt.subplots_adjust(top=0.9)
    plt.subplots_adjust(bottom=0.15)

    axs[0].hist(viral_loads, bins = 300, color='lightgrey',density=True)
    axs[0].set_xlabel('vl$_{\mathrm{in}}$ (log$_{10}$ RNA copies mL$^{-1}$)')
    axs[0].set_xlim(2, 10)
    axs[0].set_ylabel('Probability Density')

    # Viral load mean value
    mean = np.mean(viral_loads)
    axs[0].vlines(x=(mean), ymin=0, ymax=axs[0].get_ylim()[1], colors=('grey'), linestyles=('dashed'))

    # Plot colors
    hist_colors = ['lightsteelblue', 'wheat', 'darkseagreen']
    hist_vlines_colors = ['cornflowerblue', 'goldenrod', 'olivedrab']
    hist_alpha = [0.9, 0.8, 0.6]
    
    # Plot the histograms
    log10_emission_rate_dict_mean = {}
    for exp_act_key, hist_color, hist_alpha in zip(log10_emission_rate_dict, hist_colors, hist_alpha):
        exp_act_value = log10_emission_rate_dict[exp_act_key]
        axs[1].hist(exp_act_value, bins=300, color=hist_color, alpha=hist_alpha, density=True)
        # Save the mean value
        log10_emission_rate_dict_mean[exp_act_key] = np.mean(exp_act_value)

    # Max value for y axis
    ymax: float = axs[1].get_ylim()[1]

    # Plot the vlines
    for exp_act_key, vline_color in zip(log10_emission_rate_dict_mean, hist_vlines_colors):
        exp_act_mean = log10_emission_rate_dict_mean[exp_act_key]
        axs[1].vlines(x=exp_act_mean, ymin=0, ymax=ymax, colors=vline_color, alpha=0.75, linestyles='dashed')

    axs[1].set_xlabel('vR (log$_{10}$ IRP h$^{-1}$)')

    from matplotlib.patches import Patch
    import matplotlib.lines as mlines

    hist_colors_legend: typing.List = ['lightgrey'] + hist_colors
    hist_str_legend: typing.List = ['Viral Load'] + expiratory_activities

    labels = [Patch(facecolor=color, edgecolor=color, label=label)
             for color, label in zip(hist_colors_legend, hist_str_legend)]
    labels.append(mlines.Line2D([], [], color='black',
                            marker='', linestyle='dashed', label='Mean'))

    # delete y-axis from right plot 
    axs[1].set_yticklabels([])
    axs[1].set_yticks([])

    plt.legend(handles=labels, loc='upper left', bbox_to_anchor=(1, 1))
    plt.tight_layout()
    plt.show()

    return None

def short_range_emission_rate_physical_activity_comparison(first_physical_activity: str, 
                                                  second_physical_activity: str, 
                                                  expiratory_activity: str,
                                                  with_mean_aerosols: bool):
    
    first_emission_rate, _ = short_range_emission_rate_values(physical_activity=first_physical_activity, 
                                                              expiratory_activity=expiratory_activity, 
                                                              print_results=False,
                                                              with_mean_aerosols=with_mean_aerosols)
    second_emission_rate, _ = short_range_emission_rate_values(physical_activity=second_physical_activity, 
                                                               expiratory_activity=expiratory_activity, 
                                                               print_results=False,
                                                               with_mean_aerosols=with_mean_aerosols)

    print(
        'Expiratory Activity:', expiratory_activity,
        '\nFirst Physical Activity: ', first_physical_activity,
        '\nSecond Physical Activity: ', second_physical_activity, 
        '\nvR_sr_1 (first activity): ', np.mean(first_emission_rate),
        '\nvR_sr_2 (second activity): ', np.mean(second_emission_rate),
        f'\nvR_sr_1 / vR_sr_2 ({expiratory_activity}): {np.mean(first_emission_rate)/np.mean(second_emission_rate):.0f} -fold'
        '\n>>>>>>>'
        '\n'
    )

# Heatmap
def heatmap_data(exposure_model: models.ExposureModel, 
                 max_distance: float = 5., 
                 viral_dose: bool = False, 
                 risk_of_infection: bool = False, 
                 expected_new_cases: bool = True) -> None:
    
    if not (viral_dose or risk_of_infection or expected_new_cases):
        raise Exception('Please enable one of the filters: "viral_dose", "risk_of_infection" or "expected_new_cases".')
    
    if viral_dose and risk_of_infection:
        raise Exception(f'Only one of the result can be obtained. Got "viral_dose"={viral_dose} and "risk_of_infection"={risk_of_infection}.')
    
    if viral_dose and expected_new_cases:
        raise Exception(f'Only one of the result can be obtained. Got "viral_dose"={viral_dose} and "expected_new_cases"={expected_new_cases}.')
    
    if risk_of_infection and expected_new_cases:
        raise Exception(f'Only one of the result can be obtained. Got "risk_of_infection"={risk_of_infection} and "expected_new_cases"={expected_new_cases}.')

    model: models.ExposureModel = exposure_model.build_model(size=SAMPLE_SIZE)
    start, end = rep_gen.model_start_end(model)
    times: models._VectorisedFloat = rep_gen.interesting_times(model, approx_n_pts=10)
   
    distances = np.concatenate(([0.25, 0.5], np.linspace(1, max_distance, len(times) - 2)))
    result: models._VectorisedFloat = []

    oneoverln2 = 1 / np.log(2)
    infectious_dose = oneoverln2 * model.concentration_model.virus.infectious_dose

    # Calculate cumulative dose for both long- and short-range contributions
    for d in tqdm(distances):
        total_concentration_times = []
        # Replace initial distribution of distances by a specific value
        new_short_range = []
        for sr_interaction in model.short_range:
            new_short_range.append(
                models.ShortRangeModel(
                    data_registry=sr_interaction.data_registry,
                    expiration=sr_interaction.expiration,
                    activity=sr_interaction.activity,
                    presence=sr_interaction.presence,
                    distance=[d] * SAMPLE_SIZE,
                )
            )
        model_t: models.ExposureModel = dataclass_utils.nested_replace(
                model, {
                    'short_range': tuple(new_short_range),
                    'concentration_model.infected.number': 1 if expected_new_cases else model.concentration_model.infected.number,
                },
            )
        for time1, time2 in zip(times[:-1], times[1:]):
            total_concentration_times.append(np.array(model_t.deposited_exposure_between_bounds(float(time1), float(time2))).mean())

        result.append(np.cumsum(total_concentration_times))

    # Generate the results for long- and short-range contributions - for risk of infection and expected new cases (both rely on the P(I) result)
    if risk_of_infection or expected_new_cases:
        # Apply P(I) risk of infection
        for index_row, doses in enumerate(result):
            for index_column, _ in enumerate(doses):
                dose = result[index_row][index_column]
                result[index_row][index_column] = ((1 - np.exp(-((dose * (1 - model.exposed.host_immunity))/(infectious_dose *
                                                    model.concentration_model.virus.transmissibility_factor))))).mean()
                
                # Apply the multiplication factor for the expected number of new cases
                if expected_new_cases:
                    if len(model.short_range) != 0: # If short-range interactions are defined
                        result[index_row][index_column] *= np.round(model.exposed.number * 0.2) # 20% of total occupants are exposed to short-range
                    else:
                        result[index_row][index_column] *= model.exposed.number
    
    # If short-range interactions initially defined
    if len(model.short_range) != 0: 
        # Calculate dose for long-range contributions alone
        # Remove SR contributions
        model_lr = dataclass_utils.nested_replace(model, {'short_range': ()})
        # Calculate cumulative doses
        dose_lr = np.cumsum([np.array(model_lr.deposited_exposure_between_bounds(float(time1), float(time2)).mean()) for time1, time2 in zip(times[:-1], times[1:])])
        # Generate the P(I) result
        result_lr = [((1 - np.exp(-((dose_lr[i] * (1 - model.exposed.host_immunity))/(infectious_dose *
                        model.concentration_model.virus.transmissibility_factor))))).mean() for i, _ in enumerate(dose_lr)]
        
        if expected_new_cases:
            result_lr = [pi * (model.exposed.number - np.round(model.exposed.number * 0.2)) for pi in result_lr] # 20% of total occupants are exposed to short-range

    # Format data for colorbar
    factors = np.array(result).flatten()
    if viral_dose or risk_of_infection:
        custom_cmap, norm = define_colormap(factors=factors)
    elif expected_new_cases:
        custom_cmap, norm = define_colormap(factors=factors, color_scale=mpl.cm.Blues)
    
    # Generate heatmaps
    if len(model.short_range) != 0: # Short-range interactions initially defined
        # Two plots for the results
        fig, (ax_without_sr, ax) = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios': [1, len(result)]})
        # Set heatmap
        im_without_sr = ax_without_sr.imshow([result_lr], cmap=custom_cmap, norm=None if risk_of_infection or expected_new_cases else norm)
        ax_without_sr.set_yticks([0], ['LR'], fontsize = 12)

        if risk_of_infection: im_without_sr.set_clim(0, 1)
        if expected_new_cases: im_without_sr.set_clim(0, 8) # is the max for the classroom_modified scenario
    else:
        # Single plot for the result
        fig, ax = plt.subplots()

    # Short- and long-range heatmap
    im = ax.imshow(result, cmap=custom_cmap, norm=None if risk_of_infection or expected_new_cases else norm)
    if expected_new_cases:
            im.set_clim(0, 8)  # is the max for the classroom_modified scenario
    
    if risk_of_infection:
        im.set_clim(0, 1)

    # Invert the y-axis
    ax.invert_yaxis()

    ax.set_xlabel('Exposure time of the day', fontsize = 14)
    ax.set_ylabel('Distance from infector (m)', fontsize = 14)

    # x axis: ticks and labels
    total_x_ticks = np.arange(len(times)) - 0.5
    x_labels, x_ticks = [], []
    for i, t in enumerate(times):
        # If more than 4h simulation time, show only o-clock hours. Otherwise, show half-an-hour hours as well (when defined)
        if (end - start > 4) and (t % 1 == 0):
            x_labels.append(f"{int(t)}h")
            x_ticks.append(total_x_ticks[i])
        elif (end - start <= 4) and ((t % 1 == 0) or (t % 1 == 0.5)):
            hours = int(t)
            minutes = int((t - int(t)) * 60)
            x_labels.append(f"{hours:02}h{minutes:02}")
            x_ticks.append(total_x_ticks[i])
    ax.set_xticks(ticks=x_ticks, labels=x_labels, fontsize=12)

    # y axis: ticks and labels
    total_y_ticks = np.arange(len(distances))
    y_labels, y_ticks = [], []
    for i, d in enumerate(distances):
        if (d % 1 == 0):
            y_labels.append(int(d))
            y_ticks.append(total_y_ticks[i])
    ax.set_yticks(ticks=y_ticks, labels=y_labels, fontsize = 12)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            rotation_mode="anchor")

    # # Add value in each square
    # for i in range(len(distances)):
    #     for j in range(len(times[1:])):
    #         ax.text(j, i, round(result[i][j], 0),
    #                     ha="center", va="center", color="k")
    
    # Add colorbar
    cbar = plt.colorbar(im, ax=ax, orientation='vertical')

    if viral_dose:
        # Create a mask for values below and above 10 and 100
        data = np.array(result)
        mask_below_10 = data < 10
        mask_below_100 = data < 100

        # Draw lines connecting boxes where the border is 10 and 100.
        draw_border_line(ax=ax, distances=distances, times=times, mask=mask_below_10, color='grey', linestyle='-.', linewidth=1.5, label='Threshold: 10 infectious virus')
        draw_border_line(ax=ax, distances=distances, times=times, mask=mask_below_100, color='k', linestyle='-.', linewidth=1.5, label='Threshold: 100 infectious virus')

        label = 'Viral dose (infectious virus)'
        plt.legend()

    if risk_of_infection:
        # cbar.set_ticks([0.2, 0.4, 0.6, 0.8])
        # cbar.set_ticklabels([0.2, 0.4, 0.6, 0.8])
        label = 'Risk of infection'
        
    if expected_new_cases:
        # Create a mask for values below and above 10 and 100
        data = np.array(result)
        mask_below_1 = data < 1

        # Draw lines connecting boxes where the border is 10 and 100.
        # draw_border_line(ax=ax, distances=distances, times=times, mask=mask_below_1, color='grey', linestyle='-.', linewidth=1.5, label='Threshold: $R_0$ = 1')
        label = 'Expected number of new cases'
        # plt.legend()

    cbar.set_label(label, fontsize = 14)
    if risk_of_infection:
        cbar.ax.set_yticklabels(['>0.0', '0.2', '0.4', '0.6', '0.8', '<1.0'])
    cbar.ax.tick_params(labelsize = 12)
    fig.tight_layout()
    figure = plt.gcf() # get current figure
    figure.set_size_inches(5.9, 5)
    plt.show()

    return None

def jet_profile(activity_type: str, 
                expiration_type: str,
                initial_vertical_position: float = 0., 
                max_horizontal_distance: float = 2., 
                straight_jet: bool = False,
                show_initial_points: bool = False,
                ylimit_max: bool = False,
                save_png_transparent_background: bool = False):

    from matplotlib.transforms import Affine2D
    from matplotlib.patches import Ellipse

    expiration: mc.Activity = activity_distributions(data_registry)[activity_type].build_model(SAMPLE_SIZE)  # in m3/h

    # Breathing rate, from m3/h to m3/s
    BR = np.array(expiration.exhalation_rate/3600.).mean()

    # Breating cycle time (s)
    T = 4

    # Expired air volume during one breathing cycle (m3)
    Qv = T * BR

    # Exhalation coefficient. Ratio between the duration of a breathing cycle and the duration of
    # the exhalation.
    φ = 2

    # Exhalation airflow, as per Jia et al. (2022)
    Q_exh: models._VectorisedFloat = φ * BR

    # Mouth area
    mouth_diameter = 0.02  # in m
    Am: float = np.pi*(mouth_diameter**2)/4  # in m2

    # Initial velocity
    u0 = np.array(Q_exh/Am)

    # Duration of the expiration period(s), assuming a 4s breath-cycle
    tstar = T / 2.

    # Streamwise and radial penetration coefficients
    𝛽r1 = 0.18
    𝛽r2 = 0.2
    𝛽x1 = 2.4
    𝛽x2 = 2.2

    # Parameters in the jet-like stage
    # Position of virtual origin (in m)
    x0 = mouth_diameter/2/𝛽r1

    # Time of virtual origin
    t0 = (np.sqrt(np.pi)*(mouth_diameter**3))/(8*(𝛽r1**2)*(𝛽x1**2)*Q_exh)
    
    # The transition point, m
    xstar = np.array(𝛽x1*(Q_exh*u0)**0.25*(tstar + t0)**0.5 - x0)

    # Parameters in the jet-like stage
    # Position of virtual origin (in m)
    x0_puff = 𝛽r1/𝛽r2*(xstar + x0) - xstar

    # Time of virtual origin (in s)
    t0_puff = ((𝛽r1**4*𝛽x1**4)/(𝛽r2**4*𝛽x2**4))*(Q_exh/Qv)*(tstar+t0)**2 -tstar

    # horizontal width factor of the elliptical puff cloud 
    k = 9 / (4*np.pi)

    # buoyant trajectory
    # Avg temperature of the air in the room 
    ambient_temperature: float = 24 + 273.15  # in K
    # Avg temperature of the exhaled air
    mouth_temperature: float = 34 + 273.15  # in K
    C = 6.20 # from Merghani, 2023 (table 2:4: page 108) to be checked
    prandtl_number = 0.70 # from Zhivov, 1999 (ch. 4.4.1)
    psi = 0.79 # from Merghani, 2023 (table 2:4: page 108) to be checked

    def archimedes_number():
        # Ar_0 - Archimedes number
        g: float = 9.81 # in m/s2
        return (g * np.sqrt(Am) * (mouth_temperature - ambient_temperature))/(mouth_temperature * (u0**2))
    
    arch_const = archimedes_number()   
    K1 = C * np.sqrt(4/np.pi)
    K2 = K1 * np.sqrt((1 + prandtl_number)/2)
    def buoyant_trajectory(x_i):
        return (psi * (K2/K1**2) * arch_const * ((x0 + x_i)/np.sqrt(Am))**3) * np.sqrt(Am)

    def derivative_buoyant_trajectory(x_i):
        from scipy.misc import derivative
        return derivative(buoyant_trajectory, x_i, dx=1e-6)

    horizontal_distances = np.arange(0, max_horizontal_distance*1.5, 0.001)     # Please note that the 0.01 can be 0.001 for smoother colors (for testing purposes it's faster to be 0.01)
    radius = np.empty(horizontal_distances.shape, dtype=np.float64)
    # rx steady-states
    radius[horizontal_distances < xstar] = 𝛽r1*(horizontal_distances[horizontal_distances < xstar] + x0)
    # rx puff
    radius[horizontal_distances >= xstar] = 𝛽r2*(horizontal_distances[horizontal_distances >= xstar] + x0_puff)

    #jet trajectory definition
    if not straight_jet:
        trajectory = buoyant_trajectory(horizontal_distances) + initial_vertical_position
    else:
        trajectory = [initial_vertical_position] * len(horizontal_distances)

    # Plots
    fig = plt.figure()
    ax = fig.add_subplot()
    
    # jet origin
    ax.scatter(0, initial_vertical_position, color='k', s=40, label='Origin', zorder=2)

    # puff cloud (ellipse) 
    # total number of puffs to show
    num_puffs = 50
    puff_count = np.linspace(1, num_puffs, num_puffs)
    ellipse_trajectory,angle_puff = [], []
    # define the horizontal distance of each puff
    for i in puff_count:
        t = tstar*(i-1) + tstar*i
        ellipse_trajectory.append(((𝛽x2 * (Qv * u0)**0.25) * ((t + t0_puff)**0.25)) - x0_puff)
        ellipse_center_x = np.array(ellipse_trajectory)
        # calculate rotation angle perpendicular to the centerline for buoyant jet
        if not straight_jet:
            tangent_slope =  derivative_buoyant_trajectory(((𝛽x2 * (Qv * u0)**0.25) * ((t + t0_puff)**0.25)) - x0_puff)
            Perpendicular_slope = -1 / tangent_slope # negative reciprocal rule
            angle_puff.append(90. - abs(np.rad2deg(np.arctan(Perpendicular_slope))))
            angle_new = np.array(angle_puff)
        else:
            angle_new = np.zeros(len(puff_count))

    if straight_jet:
        ellipse_center_y = trajectory
        angle = 0
    else:
        ellipse_center_y = buoyant_trajectory(ellipse_center_x) + initial_vertical_position
        # calculate ellipse rotation angle
        angle = np.rad2deg(np.arctan(k/2))

    # Background concentration (long-range) in IRP/m3
    long_range_concentration = 10
    factors = short_range_factors(
        distances=horizontal_distances, 
        long_range_concentration=long_range_concentration, 
        expiration=expiration_type, 
        activity=activity_type, 
        room_humidity=0.5)
    custom_cmap, norm = define_colormap(factors)

    # jet trajectory
    ax.plot(horizontal_distances, trajectory, ls='-.', color='blue', lw=0.7, label='Centreline trajectory')

    # Calculate raise of the center line from initial_vertical_position to horizontal_distances @ 2m
    closest_index: int = np.abs(horizontal_distances - 2).argmin() # Find index of x value closest to horizontal_distances == 2m
    trajectory_at_start: float = trajectory[0]
    trajectory_at_2m: float = trajectory[closest_index]
    vertical_difference: float = trajectory_at_2m - trajectory_at_start
    print(trajectory_at_start)
    print('Vertical distance at beggining: ', round(trajectory_at_start, 3), 'm',
        '\nVertical distance when horizontal distance is 2m: ', round(trajectory_at_2m, 3), 'm',
        '\nVertical difference: ', round(vertical_difference, 3), 'm')
    # point at 2m centerline
    if not straight_jet: ax.scatter(x=2, y=trajectory_at_2m, color='k', s=20, zorder=2, marker='X', label='Vertical displacement at 2m centreline')
    # Deactivate the following lines if not needed
    # ax.plot([-0.5, 2], [trajectory_at_2m, trajectory_at_2m], linestyle='--', color='black', lw=0.7)
    # ax.plot([2, 2], [0, trajectory_at_2m], linestyle='--', color='black', lw=0.7)


    # np.savetxt('distances.csv', horizontal_distances, delimiter=',')
    # np.savetxt('trajectory.csv', trajectory, delimiter=',')

    # closest point to the xstar
    closest_x_point = np.argmin(np.abs(horizontal_distances - xstar))

    # jet profile
    if straight_jet:
        lower_straight_plot = trajectory - radius
        upper_straight_plot = trajectory + radius
        ax.plot(horizontal_distances, upper_straight_plot, ls='--', lw=0.5, color='black', label='Upper and lower bounds')
        ax.plot(horizontal_distances, lower_straight_plot, ls='--', lw=0.5, color='black')

        for i in range(len(horizontal_distances[:closest_x_point]) - 1):
            index = np.argmin(np.abs(np.array(horizontal_distances) - horizontal_distances[i]))
            plt.fill_between(horizontal_distances[i:i+2], lower_straight_plot[i:i+2], upper_straight_plot[i:i+2], 
                             color=[custom_cmap(norm(factors[index]))])
    else:
        lower_plot_x, upper_plot_x, lower_plot_y, upper_plot_y = [], [], [], []
        for d, t in zip(horizontal_distances, trajectory):
            rx = 𝛽r2*(d + x0_puff) if d >= xstar else 𝛽r1*(d + x0)
            # calculate rotation angle perpendicular to the centerline
            tangent_slope =  derivative_buoyant_trajectory(d)
            Perpendicular_slope = -1 / tangent_slope # negative reciprocal rule
            angle_t = np.rad2deg(np.arctan(Perpendicular_slope))
            angle = 90. - np.abs(angle_t)
            # if d >= xstar else np.rad2deg(np.arctan(𝛽r1))
            # calculate new coordinates of the top of the ellipse once rotated 
            upper_plot_x.append(d - rx*np.sin(np.deg2rad(angle)))
            upper_plot_y.append(t + rx*np.cos(np.deg2rad(angle))) 
            lower_plot_x.append(d + rx*np.sin(np.deg2rad(angle)))
            lower_plot_y.append(t - rx*np.cos(np.deg2rad(angle)))

        # Interpolate y-values for each x-array using numpy's interpolation
        interpolated_y1 = np.interp(horizontal_distances, lower_plot_x, lower_plot_y)
        interpolated_y2 = np.interp(horizontal_distances, upper_plot_x, upper_plot_y)

        # Find the index where the gradient falls below the threshold for each line and plot the lines
        flat_index2 = np.argmax(np.abs(np.gradient(interpolated_y2)) < 1e-10)
        ax.plot(horizontal_distances, interpolated_y1, ls='--', lw=0.5, color='black', label='Upper and lower bounds')
        ax.plot(horizontal_distances[:flat_index2], interpolated_y2[:flat_index2], ls='--', lw=0.5, color='black')
        
        # truncate the fill between
        # coordinates of edges of first ellipse
        x, y = ellipse_center_x[0], ellipse_center_y[0]
        center = (xstar, trajectory[closest_x_point])
        rx = 𝛽r1*(xstar + x0)
        lower_translated_point = Affine2D().scale(-1, -1).translate(*center).rotate_deg(abs(angle_new[0])).scale(-1, -1).translate(*center).transform((x, y - rx))
        upper_translated_point = Affine2D().scale(-1, -1).translate(*center).rotate_deg(abs(angle_new[0])).scale(-1, -1).translate(*center).transform((x, y + rx))
        closest_upper_x_point_index = np.argmin(np.abs(np.array(horizontal_distances) - upper_translated_point[0]))
        closest_lower_x_point_index = np.argmin(np.abs(np.array(horizontal_distances) - lower_translated_point[0]))
        # Generate points
        x_points = horizontal_distances[closest_upper_x_point_index:closest_lower_x_point_index]
        x1 = upper_translated_point[0]
        y1 = upper_translated_point[1]
        x2 = lower_translated_point[0]
        y2 = lower_translated_point[1]
        y_points = y1 + ((y2 - y1) / (x2 - x1)) * (x_points - x1)

        points = [(x, y) for x, y in zip(x_points, y_points)]
        for (point_x, point_y) in points:
            interpolated_y2[np.argmin(np.abs(np.array(horizontal_distances) - point_x))] = point_y

        for i in range(len(horizontal_distances[:closest_lower_x_point_index]) - 1):
            index = np.argmin(np.abs(np.array(horizontal_distances) - horizontal_distances[i]))
            plt.fill_between(horizontal_distances[i:i+2], interpolated_y1[i:i+2], interpolated_y2[i:i+2], 
                             color=[custom_cmap(norm(factors[index]))])
 
    # Ellipsis
    for i, (x, y, a) in enumerate(zip(ellipse_center_x, ellipse_center_y, angle_new)):
        # Index of the distance to retrieve the factor
        index = np.argmin(np.abs(np.array(horizontal_distances) - x))
        facecolor = custom_cmap(norm(factors[index]))[:3]
        rx = 𝛽r2*(x + x0_puff)
        if (i == 0 or i == len(angle_new) - 1):
            ells = Ellipse(xy=(x, y), width=k*rx, height=2*rx, fill=True, 
                        facecolor=facecolor, angle=abs(a), 
                        lw=0.5, edgecolor='black', ls='--')
        else:
            ells = Ellipse(xy=(x, y), width=k*rx, height=2*rx, fill=True, 
                        facecolor=facecolor, angle=abs(a))
        ax.add_patch(ells)
        
    # # benchmark points
    # if show_initial_points:
    #     # Initial data points
    #     x = [0.01472, 0.05652, 0.10305, 0.14367, 0.17616, 0.20289, 0.25163, 0.30848, 0.35146, 0.40849, 0.45841,
    #         0.5024, 0.54657, 0.58143, 0.61155, 0.65233, 0.68482, 0.72898, 0.75453, 0.77179, 0.79041, 0.80784, 0.8229]
    #     z_c = np.array([0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.01, 0.01, 0.01, 0.02,
    #         0.03, 0.03, 0.04, 0.05, 0.06, 0.06, 0.08, 0.09, 0.09, 0.10, 0.11, 0.11]) + initial_vertical_position
    #     i_dont_know = [el*10 for el in z_c]
    #     plt.scatter(x, z_c, s=2, label=f'Initial points (initial vertical position: {initial_vertical_position} m)')    

    # Set x-axis limits
    y1 = trajectory[closest_x_point]*1.1
    y0 = trajectory[closest_x_point]*0.9
    center = (xstar, trajectory[closest_x_point])
    translated_lower_point = Affine2D().scale(-1, -1).translate(*center).rotate_deg(abs(angle_new[0])).scale(-1, -1).translate(*center).transform((xstar, y1))
    translated_upper_point = Affine2D().scale(-1, -1).translate(*center).rotate_deg(abs(angle_new[0])).scale(-1, -1).translate(*center).transform((xstar, y0))
    ax.plot([translated_lower_point[0], translated_upper_point[0]], [translated_lower_point[1], translated_upper_point[1]], ls='--', lw = 0.7,
                color='salmon')
    
    # Set aspect ratio
    ax.set_aspect('auto')

    #annotation
    tab = "     "
    if straight_jet:
        ax.annotate(f'Scenario:\n{tab}- {expiration_type}\n{tab}- {activity_type} ({np.round(u0, 0)} m/s)\n{tab}- {num_puffs} puffs\n{tab}- Expiration time of {round((tstar + T*num_puffs)/60)} min', (0.2, 0.5) )
    else:
        ax.annotate(f'Scenario:\n{tab}- {expiration_type}\n{tab}- {activity_type} ({np.round(u0, 0)} m/s)\n{tab}- {num_puffs} puffs\n{tab}- Expiration time of {round((tstar + T*num_puffs)/60)} min\n{tab}- Exhaled air temp of {round(mouth_temperature - 273.15)} $^o$C\n{tab}- Room temp of {round(ambient_temperature - 273.15)} $^o$C', (1, 0.3) )

    if ylimit_max:
        # hard limit of the y max
        ylimit = [0, 4]
    else:
        if straight_jet:
            ylimit = [0, max(upper_straight_plot) * 1.2]
        else:
            ylimit = [0, max(upper_plot_y) * 1.2]
        # xlimit = [0, len(distances)*1.2]

    sm = plt.cm.ScalarMappable(cmap=custom_cmap, norm=norm)
    sm.set_array([])
    plt.colorbar(sm, ax=ax, label='Viral Concentration (IRP/m³)')

    plt.ylim(ylimit)
    plt.xlim([-0.5, max_horizontal_distance*1.05])
    
    # Get the current tick locations and labels
    ticks = plt.xticks()[0]
    labels = plt.xticks()[1]
    # Remove the first tick
    new_ticks = ticks[1:]
    new_labels = labels[1:]
    
    # Set the modified ticks and labels
    plt.xticks(new_ticks, new_labels)

    plt.title('Jet profile')
    plt.xlabel('Horizontal distance (m)')
    plt.ylabel('Vertical distance (m)')
    
    # Legend
    # reordering the labels 
    handles, labels = plt.gca().get_legend_handles_labels() 
    if straight_jet:
        order = [0, 1, 2]
    else:
        order = [0, 2, 1, 3] 
     
    plt.legend([handles[i] for i in order], [labels[i] for i in order], loc='upper left') 

    if save_png_transparent_background:
        plt.savefig('puff_jet.png', transparent=True, dpi=300)

    plt.show()

    print('Mouth diameter:', mouth_diameter, 'm', 
        '\nBreathing rate:', BR, 'm3/s',
        '\nInitial velocity:', u0, 'm/s', 
        '\nBreathing cycle:', T, 's',
        '\nJet puff transition:', xstar, 'm;', tstar, 's',
        )
    print('t0:', t0, 't0prime:',t0_puff)
    print('x0:', x0, 'x0prime:', x0_puff)
    print('Ellipse horizontal distances (m):', ellipse_center_x)

    return
