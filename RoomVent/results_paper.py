""" Title: CAiMIRA - CERN Airborne Model for Indoor Risk Assessment
Author: A. Henriques et al
Date: 08/02/2023
Code version: 4.14
Availability: https://gitlab.cern.ch/caimira/caimira-publications """

from scripts_paper import *
from model_scenarios_paper import *

# Plot the CO2 concentration line (generated vs collected) office
transition_times, T_values, C_values, model = simulation_CO2_day_office()
generate_co2_curve(transition_times, T_values, C_values, model)

# Plot the CO2 concentration line (generated vs collected) meeting room
transition_times, T_values, C_values, model = simulation_CO2_day_meeting()
generate_co2_curve_meeting_room(transition_times, T_values, C_values, model)

# Plot the CO2 concentration line (generated vs collected) meeting room with ventilation
transition_times, T_values, C_values, model = simulation_CO2_day_meeting_vent()
generate_co2_curve_meeting_room_vent(transition_times, T_values, C_values, model)