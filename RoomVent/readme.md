## Obtaining the results

Once the code is properly installed on your machine, you can now run the script to reproduce the results.
All the methods are documented with their respective `docstrings`.

<!-- ### Reproduce one specific figure -->

Note that by **default** all the results are "commented out" for performance reasons. In order to generate a specific result or figure, uncomment the respective generation code by following the steps below (under the respective paper's folder):

- Open the `results_paper.py` file.
  **Note1**: For macOS we recommend to use `IDLE` code editor: right click on the file and select `Open with` and choose `IDLE`.
  **Note2**: For windows we recommend to simply use `Notepad` or other code editor application for pyhton.

- Uncomment the desired code lines for a specific method, by removing the `#` tag before and after the method call.

- For macOS, simply click F5 (Run Module) to run the code if you are using `IDLE`. For windows, simply double click on `results_paper.py`.

- The desired figure or result is generated and it will be displayed in a new window or directly in the terminal.

## Disclaimer

CAiMIRA has not undergone review, approval or certification by competent authorities, and as a result, it cannot be considered as a fully endorsed and reliable tool, namely in the assessment of potential viral emissions from infected hosts to be modelled.

The software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and non-infringement.
In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.