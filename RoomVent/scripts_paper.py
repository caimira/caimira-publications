""" Title: CAiMIRA - CERN Airborne Model for Indoor Risk Assessment
Author: A. Henriques et al
Date: 21/12/2023
Code version: 4.14.3
Availability: https://gitlab.cern.ch/caimira/caimira-publications """

from caimira import models
import caimira.monte_carlo as mc
from caimira.monte_carlo.data import virus_distributions, covid_overal_vl_data
from caimira import dataclass_utils
import caimira.apps.calculator.report_generator as rep_gen
from caimira.dataclass_utils import nested_replace
from tqdm import tqdm

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib as mpl
import typing
import dataclasses

from model_scenarios_paper import SimpleCO2ConcentrationModel

SAMPLE_SIZE = 250_000

############# AUX ############


def define_colormap(cns):
    min_val, max_val = 0.2, 1
    n = 100
    orig_cmap = mpl.cm.Reds
    colors = orig_cmap(np.linspace(min_val, max_val, n))

    norm = mpl.colors.Normalize(vmin=cns.min(), vmax=cns.max())
    cmap = mpl.cm.ScalarMappable(
        norm=norm, cmap=mpl.colors.LinearSegmentedColormap.from_list("mycmap", colors))
    cmap.set_array([])

    return cmap


def c_model_start_end(c_model: models.CO2ConcentrationModel):
    t_start = c_model.CO2_emitters.presence.boundaries()[0][0]
    t_end = c_model.CO2_emitters.presence.boundaries()[-1][1]
    return t_start, t_end


def fill_big_gaps(array, gap_size):
    """
    Insert values into the given sorted list if there is a gap of more than ``gap_size``.
    All values in the given array are preserved, even if they are within the ``gap_size`` of one another.

    >>> fill_big_gaps([1, 2, 4], gap_size=0.75)
    [1, 1.75, 2, 2.75, 3.5, 4]

    """
    result = []
    if len(array) == 0:
        raise ValueError("Input array must be len > 0")

    last_value = array[0]
    for value in array:
        while value - last_value > gap_size + 1e-15:
            last_value = last_value + gap_size
            result.append(last_value)
        result.append(value)
        last_value = value
    return result


def non_temp_transition_times(model: models.ExposureModel):
    """
    Return the non-temperature (and PiecewiseConstant) based transition times.

    """
    def walk_model(model, name=""):
        # Extend walk_dataclass to handle lists of dataclasses
        # (e.g. in MultipleVentilation).
        for name, obj in dataclass_utils.walk_dataclass(model, name=name):
            if name.endswith('.ventilations') and isinstance(obj, (list, tuple)):
                for i, item in enumerate(obj):
                    fq_name_i = f'{name}[{i}]'
                    yield fq_name_i, item
                    if dataclasses.is_dataclass(item):
                        yield from dataclass_utils.walk_dataclass(item, name=fq_name_i)
            else:
                yield name, obj

    t_start, t_end = c_model_start_end(model)

    change_times = {t_start, t_end}
    for name, obj in walk_model(model, name="exposure"):
        if isinstance(obj, models.Interval):
            change_times |= obj.transition_times()

    # Only choose times that are in the range of the model (removes things
    # such as PeriodicIntervals, which extend beyond the model itself).
    return sorted(time for time in change_times if (t_start <= time <= t_end))


def interesting_times(model: models.CO2ConcentrationModel, approx_n_pts=100) -> typing.List[float]:
    """
    Pick approximately ``approx_n_pts`` time points which are interesting for the
    given model.

    Initially the times are seeded by important state change times (excluding
    outside temperature), and the times are then subsequently expanded to ensure
    that the step size is at most ``(t_end - t_start) / approx_n_pts``.

    """
    times = non_temp_transition_times(model)

    # Expand the times list to ensure that we have a maximum gap size between
    # the key times.
    nice_times = fill_big_gaps(times, gap_size=(
        max(times) - min(times)) / approx_n_pts)
    return nice_times

############# METHODS ############

def generate_co2_curve(transition_times, T_values, C_values, concentration_model: models.CO2ConcentrationModel):
    # Plot the collected data
    day_times = transition_times[:-1]
    day_co2 = C_values

    # plt.scatter(day_times, day_co2, marker='.', s=15,
    #             color='k', label="Sensor data", zorder=2)
    plt.plot(day_times, day_co2, label="Experimental data", color="k", zorder=2, lw=2)

    # scenario_times = interesting_times(concentration_model)
    scenario_times = day_times # have way more data points to calculate the errors
    concentrations = np.array([
        np.array(concentration_model.concentration(float(time))).mean()
        for time in scenario_times
    ])

    plt.plot(scenario_times, concentrations, lw=2, color='salmon', ls='--',
            label="CAiMIRA model (baseline)", zorder=1)

    plt.xticks(plt.xticks()[0], np.array(
        [str(int(x)) + ':' + "{:02d}".format(int((x*60) % 60)) for x in plt.xticks()[0]]))

    # Fitting algorithm
    ventilation_transition_times = (14.0, 14.25, 14.78, 15.10, 15.53, 15.87, 16.51, 16.84, 17.50)
    # Adding vertical dashed lines at ventilation_transition_times
    intervention_times = ventilation_transition_times[:-1] # ventilation minus the end of the experiment
    for time_point in intervention_times:
        plt.axvline(x=time_point, linestyle='--', color='gray', linewidth=1)
    plt.annotate('1', xy=(intervention_times[0]*1.001, 1400), fontsize=10)
    plt.annotate('8', xy=(intervention_times[7]*1.001, 1400), fontsize=10)

    data_model = models.CO2DataModel(
        data_registry=concentration_model.data_registry,
        room_volume=37.5*2.73,
        number=4,
        presence=concentration_model.population.presence,
        ventilation_transition_times=ventilation_transition_times,
        times=day_times,
        CO2_concentrations=day_co2,
    )

    fit_parameters = data_model.CO2_fit_params()
    exhalation_rate = fit_parameters['exhalation_rate']
    ventilation_values = fit_parameters['ventilation_values']
    print('#### Results ####')
    print('Exhalation rate: ', exhalation_rate)
    print('Ventilation values (ACH): ', ventilation_values)
    
    CO2_concentrations = SimpleCO2ConcentrationModel(
        data_registry=concentration_model.data_registry,
        room=concentration_model.room,
        ventilation=models.CustomVentilation(models.PiecewiseConstant(
            ventilation_transition_times, ventilation_values)),
        CO2_emitters=models.SimplePopulation(
            number=concentration_model.CO2_emitters.number,
            presence=concentration_model.CO2_emitters.presence,
            activity=models.Activity(
                exhalation_rate=exhalation_rate, inhalation_rate=exhalation_rate),
        ),
        # CO2_atmosphere_concentration=488.4555556,
        CO2_atmosphere_concentration=440.44,
        CO2_initial_concentration=day_co2[0]
    )
    predictive_CO2_after_fitting = np.array([CO2_concentrations.concentration(time) for time in scenario_times])

    plt.plot(scenario_times, predictive_CO2_after_fitting, lw=2, color='royalblue',
                label="Predicted model after fitting (study)", zorder=1)
    
    # Errors - RMSPE
    rmspe = np.sqrt(np.mean(((day_co2 - concentrations) / day_co2) ** 2))
    print('RMSPE before fitting = ', rmspe * 100, ' %')
    rmspe = np.sqrt(np.mean(((day_co2 - predictive_CO2_after_fitting) / day_co2) ** 2))
    print('RMSPE after fitting = ', rmspe * 100, ' %')

    plt.annotate('Office scenario:', xy=(ventilation_transition_times[0], 1600), fontsize=12, backgroundcolor="w")    

    plt.legend()
    plt.ylim(400, 1800)
    plt.xlabel('Time of day', fontsize=12)
    plt.ylabel('CO$_2$ concentration (ppm)', fontsize=12)
    plt.savefig("office.png", format="png", dpi=1200)
    plt.show()


def generate_co2_curve_meeting_room(transition_times, T_values, C_values, concentration_model: models.CO2ConcentrationModel):
    # Plot the collected data
    day_times = transition_times[:-1]
    day_co2 = C_values

    plt.plot(day_times, day_co2, label="Experimental data", color="k", zorder=2, lw=2)

    scenario_times = day_times # have way more data points to calculate the errors
    concentrations = np.array([
        np.array(concentration_model.concentration(float(time))).mean()
        for time in scenario_times
    ])

    plt.plot(scenario_times, concentrations, lw=2, color='salmon', ls='--',
            label="CAiMIRA model (baseline)", zorder=1)

    plt.xticks(plt.xticks()[0], np.array(
        [str(int(x)) + ':' + "{:02d}".format(int((x*60) % 60)) for x in plt.xticks()[0]]))

    # Fitting algorithm
    ventilation_transition_times = (9, 10.37, 11.07, 11.75)
    # Adding vertical dashed lines at ventilation_transition_times
    for time_point in ventilation_transition_times[1:-1]:
        plt.axvline(x=time_point, linestyle='--', color='gray', linewidth=1)
    
    person_transition_times = (9.08, 9.33, 9.75, 10.75)
    for time_point in person_transition_times:
        plt.axvline(x=time_point, linestyle='--', color='gray', linewidth=1)

    plt.annotate('1', xy=(person_transition_times[0]*1.001, 800), fontsize=10)
    plt.annotate('6', xy=(ventilation_transition_times[2]*1.001, 1070), fontsize=10)

    data_model = models.CO2DataModel(
        data_registry=concentration_model.data_registry,
        room_volume=32.2*2.58, # 83.076
        number=models.IntPiecewiseConstant(
            transition_times=tuple([9, 9.08, 9.33, 9.75, 10.75, 11.75]), 
            values=tuple((0, 2, 3, 2, 3))),
        presence=None,
        ventilation_transition_times=ventilation_transition_times,
        times=day_times,
        CO2_concentrations=day_co2,
        # exhalation_transition_times=(9.08, 9.33, 9.75, 10.75, 11.75),
    )

    fit_parameters = data_model.CO2_fit_params()
    exhalation_rate = fit_parameters['exhalation_rate']
    ventilation_values = fit_parameters['ventilation_values']
    print('#### Results ####')
    print('Exhalation rate: ', exhalation_rate)
    print('Ventilation values (ACH): ', ventilation_values)
    
    CO2_concentrations = SimpleCO2ConcentrationModel(
        data_registry=concentration_model.data_registry,
        room=concentration_model.room,
        ventilation=models.CustomVentilation(models.PiecewiseConstant(
            ventilation_transition_times, ventilation_values)),
        CO2_emitters=models.SimplePopulation(
            number=concentration_model.CO2_emitters.number,
            presence=concentration_model.CO2_emitters.presence,
            # activity=models.ActivityPiecewiseConstant(
            #     transition_times=(9.08, 9.33, 9.75, 10.75, 11.75),
            #     values=tuple([models.Activity(exh_val, exh_val) for exh_val in exhalation_rate])
            # ),
            activity=models.Activity(exhalation_rate, exhalation_rate),
        ),
        CO2_atmosphere_concentration=440.44,
        CO2_initial_concentration=day_co2[0],
    )
    predictive_CO2_after_fitting = np.array([CO2_concentrations.concentration(time) for time in scenario_times])

    plt.plot(scenario_times, predictive_CO2_after_fitting, lw=2, color='royalblue',
                label="Predicted model after fitting (study)", zorder=1)
    
    # Errors - RMSPE
    rmspe = np.sqrt(np.mean(((day_co2 - concentrations) / day_co2) ** 2))
    print('RMSPE before fitting = ', rmspe * 100, ' %')
    rmspe = np.sqrt(np.mean(((day_co2 - predictive_CO2_after_fitting) / day_co2) ** 2))
    print('RMSPE after fitting = ', rmspe * 100, ' %')

    plt.annotate('Meeting room scenario:', xy=(ventilation_transition_times[0], 1000), fontsize=12, backgroundcolor="w")    
    
    plt.legend()
    plt.ylim(400, 1300)
    plt.xlabel('Time of day', fontsize=12)
    plt.ylabel('CO$_2$ concentration (ppm)', fontsize=12)
    plt.savefig("meeting.png", format="png", dpi=1200)
    plt.show()

def generate_co2_curve_meeting_room_vent(transition_times, T_values, C_values, concentration_model: models.CO2ConcentrationModel):
    # Plot the collected data
    day_times = transition_times[:-1]
    day_co2 = C_values

    fig = plt.figure()

    # Create grid layout with 70% for the main plot and 30% for the error plot
    gs = fig.add_gridspec(2, 1, height_ratios=[7, 3], width_ratios=[1])

    # Main plot
    ax1 = fig.add_subplot(gs[0])
    ax1.plot(day_times, day_co2, label="Experimental data", color="k", zorder=2, lw=2)
    scenario_times = day_times
    concentrations = np.array([
        np.array(concentration_model.concentration(float(time))).mean()
        for time in scenario_times
    ])
    ax1.plot(scenario_times, concentrations, lw=2, color='salmon', ls='--',
            label="CAiMIRA model (baseline)", zorder=1)

    ventilation_transition_times = (13.67, 14.37, 14.72, 15.00, 15.33, 15.68, 16.03, 16.67)
    presence_transition_times = (13.67, 13.75, 13.88, 16.67)
    data_model = models.CO2DataModel(
        data_registry=concentration_model.data_registry,
        room_volume=32.2*2.58, # 83.076
        number=models.IntPiecewiseConstant(transition_times=presence_transition_times, values=(2, 3, 4)),
        presence=None,
        ventilation_transition_times=ventilation_transition_times,
        times=day_times,
        CO2_concentrations=day_co2,
        # exhalation_transition_times=None,
    )

    fit_parameters = data_model.CO2_fit_params()
    exhalation_rate = fit_parameters['exhalation_rate']
    ventilation_values = fit_parameters['ventilation_values']
    print('#### Results ####')
    print('Exhalation rate: ', exhalation_rate)
    print('Ventilation values (ACH): ', ventilation_values)

    CO2_concentrations = SimpleCO2ConcentrationModel(
        data_registry=concentration_model.data_registry,
        room=concentration_model.room,
        ventilation=models.CustomVentilation(models.PiecewiseConstant(
            ventilation_transition_times, ventilation_values)),
        CO2_emitters=models.SimplePopulation(
            number=concentration_model.CO2_emitters.number,
            presence=concentration_model.CO2_emitters.presence,
            activity=models.Activity(exhalation_rate, exhalation_rate),
        ),
        CO2_atmosphere_concentration=440.44,
        CO2_initial_concentration=day_co2[0],
    )
    predictive_CO2_after_fitting = np.array([CO2_concentrations.concentration(time) for time in scenario_times])

    ax1.plot(scenario_times, predictive_CO2_after_fitting, lw=2, color='royalblue',
                label="Predicted model after fitting (study)", zorder=1)
    ax1.legend(loc='upper right')
    ax1.set_ylabel('CO$_2$ concentration (ppm)')
    ax1.set_ylim(400, 1000)
    
    # Error plot
    ax2 = fig.add_subplot(gs[1])
    window_size = 5  # Adjust the window size as needed

    percentage_error = np.sqrt(((day_co2 - concentrations) / day_co2)**2) * 100
    smoothed_percentage_error = np.convolve(percentage_error, np.ones(window_size)/window_size, mode='same')
    ax2.plot(day_times, smoothed_percentage_error, label='Percentage Error (before fitting)', color='salmon', ls='--')

    percentage_error = np.sqrt(((day_co2 - predictive_CO2_after_fitting) / day_co2)**2) * 100
    smoothed_percentage_error = np.convolve(percentage_error, np.ones(window_size)/window_size, mode='same')
    ax2.plot(day_times, smoothed_percentage_error, label='Percentage Error (after fitting)', color='royalblue')

    ax2.set_ylabel('RMSE (%)')
    ax2.set_xlabel('Time of day', fontsize=12)
    ax2.set_ylim(0, 35)
    
    ax1.set_xticklabels([])
    ax2.set_xticklabels([str(int(x)) + ':' + "{:02d}".format(int((x*60) % 60)) for x in plt.xticks()[0]])
    # ax2.legend(loc='upper right')

    for time_point in ventilation_transition_times[1:-1]:
        ax1.axvline(x=time_point, linestyle='--', color='gray', linewidth=1)
        ax2.axvline(x=time_point, linestyle='--', color='gray', linewidth=1)

    for time_point in presence_transition_times[:3]:
        ax1.axvline(x=time_point, linestyle='--', color='gray', linewidth=1)
        ax2.axvline(x=time_point, linestyle='--', color='gray', linewidth=1)

    # Annotations
    ax1.annotate('1', xy=(presence_transition_times[0]*1.0005, 800), fontsize=10)
    ax1.annotate('9', xy=(ventilation_transition_times[-2]*1.001, 750), fontsize=10)
    
    # Errors - RMSPE
    rmspe = np.sqrt(np.mean(((day_co2 - concentrations) / day_co2) ** 2))
    print('RMSPE before fitting = ', rmspe * 100, ' %')

    rmspe = np.sqrt(np.mean(((day_co2 - predictive_CO2_after_fitting) / day_co2) ** 2))
    print('RMSPE after fitting = ', rmspe * 100, ' %')

    ax1.annotate('Meeting room \nscenario 2:  ', xy=(presence_transition_times[0], 875), fontsize=12, backgroundcolor="w")
    plt.savefig("meeting_with_vent.png", format="png", dpi=1200)
    plt.show()
